/** Dynamic Table v1.0
 * Copyright 2017 William Aruan
 */

(function( $ ) {

    $.fn.DynamicTable = function(options) {
 		var defaultOptions = {
 			tabData: {},
 			noDataText: "Tidak ada data",
 			delButtonText: "Hapus",
 			delButtonClass: "btn btn-block btn-danger",
 			dataIdName: "Id",
 			hasFirstRow: false,
 			delFunc: null
 		};
 		var mergeOption;
 		if(options) { mergeOption = $.extend({}, defaultOptions, options) }
 		else { mergeOption = defaultOptions; }

        return new _dynamicApi(this, mergeOption);
 
    };

    /*
	 *	Function to initialize the table
     */
    function _initialize() {
    	_checkData.call(this);
    }

    /*
	 *	Function to check if any data remaining in the table
     */
    function _checkData() {
    	var self = this;
    	var $th = $(this.element).find("thead th");
    	var size = $th.length;
		var $row = $("<tr/>");
		
		if(this.tableData.length == 0) {
			var $tbody = $(this.element).find("tbody tr");
			if(self.options.hasFirstRow) { $tbody.slice(1).remove(); }
			else { $tbody.remove(); }
			
			$row.append($("<td colspan='" + size + "' style='text-align:center;' id='row-nodata' />").html(self.options.noDataText));
			$(this.element).append($row);
		} else {
			$row = $('#row-nodata');
			$row.remove();
		}
    }

    /*
     * Function to Generate Row when Data had been inserted
     */
    function _generateRow(datas) {
    	var self = this;
    	//Creating Row for TableBody
    	var $row = $("<tr/>");

    	//Iterate Row and Create Column(TD) inside the Row
    	$.each(datas, function(key, val) {
    		if(key !== self.options.dataIdName)
    	    	$row.append($("<td/>").html(val));
    	    else
    	    	$row.append($('<input type="hidden" name="dynamicchild_id" id="dynamicchild-id" />').val(val));
    	});

    	//Append Delete Button to the Row
    	var $button = $('<button type="button" id="remove" class="' + self.options.delButtonClass + ' btn-child" data-dynamicid = "'+ datas[self.options.dataIdName] + '" />').html(self.options.delButtonText);
    	$row.append($("<td />").html($button));

    	//Append Row to tablebody
    	$(this.element).append($row);
    	var btnfunc;

    	if(this.options.delFunc) {
    		btnfunc = this.options.delFunc;
    	} else {
    		btnfunc = function() {};
    	}

    	//Bind Delete Event for Delete Button
    	$button.on("click", function() {
    		self.remove($(this).data('dynamicid'), btnfunc.call(self, datas));
    	});

    	//Update the Table
    	_initialize.call(this);

    }

    /*
     *	Create Class that Contains all Method for the Table
     */
    var _dynamicApi = function(element, options) {
    	this.element = element;
		this.tableData = [];
		this.options = options;
		_initialize.call(this);
    };

    /*
     *	Create The Method for the Dynamic Api
     */
    $.extend( _dynamicApi.prototype, {
    	add: function(datas, callback) {
    		var self = this;
			if(datas[this.options.dataIdName] === null || datas[this.options.dataIdName] === undefined ) {
				datas[this.options.dataIdName] = this.tableData.length + 1;
			}
			var dataToInsert = $.extend({}, self.options.tabData, datas);
			this.tableData.push(dataToInsert);
			_generateRow.call(this, dataToInsert);

			if (typeof callback === "function") {
				callback.call(this, dataToInsert);
			}
    	},

    	addMany: function(datas, typeData) {
    		var self = this;
    		if(typeData == "json") {
    			datas = $.parseJSON(datas);
    		}
    		$.each(datas, function(key, val) {
    			//console.log(val);
    			if(val[self.options.dataIdName] === null || val[self.options.dataIdName] === undefined ) {
    				val[self.options.dataIdName] = self.tableData.length + 1;
    			}
    			var dataToInsert = $.extend({}, self.options.tabData, val);
    			self.tableData.push(dataToInsert);
    			_generateRow.call(self, dataToInsert);
	    	});
    	},

    	remove: function(id, callback) {
    		var datas;
    		var idname = this.options.dataIdName;
    		var index = $.map(this.tableData, function(obj, index) {
    					    if(obj[idname] === id) {
    					    	datas = obj;
    					        return index;
    					    }
    					});
    		if(index > -1) {
    			this.tableData.splice(index, 1);
    			$('.btn-child[data-dynamicid="'+id+'"]').parents('tr').remove();
    			_initialize.call(this);
    		}

    		if(typeof callback === "function") {
    			callback.call(this, datas);
    		}
    	},

    	clear: function(callback) {
    		this.tableData = [];
    		console.log(this.tableData);
    		_initialize.call(this);

    		if(typeof callback === "function") {
    			callback.call(this);
    		}
    	},

    	getData: function() {
    		return this.tableData;
    	},

    	setDelFunc: function(func) {
    		this.options.delFunc = func;
    	}
    });
 
}( jQuery ));