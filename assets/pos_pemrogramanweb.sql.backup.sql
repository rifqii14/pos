/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : pos_pemrogramanweb

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 30/12/2019 00:00:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (5, 'Makanan', '2019-09-07 13:34:50');
INSERT INTO `categories` VALUES (9, 'Cemilan', '2019-09-30 14:10:07');
INSERT INTO `categories` VALUES (11, 'Obat', '2019-10-02 20:11:07');
INSERT INTO `categories` VALUES (15, 'Minuman', '2019-10-02 21:52:14');

-- ----------------------------
-- Table structure for discounts
-- ----------------------------
DROP TABLE IF EXISTS `discounts`;
CREATE TABLE `discounts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `discount` double NOT NULL,
  `create` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of discounts
-- ----------------------------
INSERT INTO `discounts` VALUES (1, 'No discount', 1, '2019-09-30 23:16:30');
INSERT INTO `discounts` VALUES (2, 'Promo Weekend Discount 10%', 0.9, '2019-09-28 17:39:51');
INSERT INTO `discounts` VALUES (3, 'Promo Rabu Ceria Discount 20%', 0.8, '2019-09-28 17:40:31');
INSERT INTO `discounts` VALUES (5, 'Promo JuSa Discount 15%', 0.85, '2019-10-04 09:40:28');
INSERT INTO `discounts` VALUES (17, 'Promo JuSa Discount 45%', 0.55, '2019-10-10 10:16:13');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `categories_id` int(11) NOT NULL,
  `barcode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `costprice` double NOT NULL,
  `sellingprice` double NOT NULL,
  `discounts_id` int(11) NOT NULL,
  `finalprice` double NOT NULL,
  `stock` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `create` datetime(0) NOT NULL,
  `delete` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (27, 'Indomie Kari Ayam', 5, '8976251402917', '/img/product/product_1576167806.jpg', 78000, 90000, 1, 90000, 139, 2, '2019-12-12 23:23:26', NULL);
INSERT INTO `products` VALUES (28, 'Sirup Marjan Pandan', 15, '98763518', '/img/product/product_1576168282.jpg', 8000, 10000, 1, 10000, 140, 1, '2019-12-12 23:31:22', NULL);
INSERT INTO `products` VALUES (29, 'Lays Rasa Rumput Laut', 9, '123456', '/img/product/product_1577088227.jpg', 8000, 13000, 1, 13000, 1, 1, '2019-12-23 15:03:47', NULL);
INSERT INTO `products` VALUES (30, 'Indomie Soto', 5, '89686010343', '/img/product/product_1577638658.png', 1500, 3000, 1, 3000, 0, 1, '2019-12-29 23:57:38', NULL);

-- ----------------------------
-- Table structure for products_adjustment
-- ----------------------------
DROP TABLE IF EXISTS `products_adjustment`;
CREATE TABLE `products_adjustment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `stock_adjustment` int(11) NOT NULL,
  `total_price` double NOT NULL,
  `date` datetime(0) NOT NULL,
  `create` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products_adjustment
-- ----------------------------
INSERT INTO `products_adjustment` VALUES (34, 28, 10, 80000, '2019-12-12 23:32:01', '2019-12-12 23:32:01');
INSERT INTO `products_adjustment` VALUES (35, 27, 2, 156000, '2019-12-22 23:23:25', '2019-12-22 23:23:25');
INSERT INTO `products_adjustment` VALUES (36, 27, 3, 234000, '2019-12-23 14:00:28', '2019-12-23 14:00:28');
INSERT INTO `products_adjustment` VALUES (37, 28, 10, 80000, '2019-12-23 14:01:06', '2019-12-23 14:01:06');
INSERT INTO `products_adjustment` VALUES (38, 29, 10, 80000, '2019-12-23 15:04:03', '2019-12-23 15:04:03');

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_reference` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `users_id` int(11) NOT NULL,
  `date` datetime(0) NOT NULL,
  `create` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES (1, 'VMDODBO5', 6, '2019-12-17 15:57:49', '2019-12-17 15:57:49');
INSERT INTO `transactions` VALUES (2, 'FON8285T', 6, '2019-10-19 11:44:29', '2019-10-19 11:44:29');
INSERT INTO `transactions` VALUES (3, 'DJ6A228J', 6, '2019-11-23 11:55:19', '2019-11-23 11:55:19');
INSERT INTO `transactions` VALUES (4, '8SF94SXW', 6, '2019-12-22 23:26:50', '2019-12-22 23:26:50');
INSERT INTO `transactions` VALUES (5, 'F8G5NF3N', 6, '2019-12-23 11:57:08', '2019-12-23 11:57:08');
INSERT INTO `transactions` VALUES (6, 'PGTE1EGG', 6, '2019-12-23 13:58:38', '2019-12-23 13:58:38');
INSERT INTO `transactions` VALUES (7, 'Z799EXB8', 6, '2019-12-23 15:04:38', '2019-12-23 15:04:38');
INSERT INTO `transactions` VALUES (8, 'RRNBS33O', 6, '2019-12-23 15:26:46', '2019-12-23 15:26:46');
INSERT INTO `transactions` VALUES (10, 'YOJZCR03', 8, '2019-12-26 23:31:55', '2019-12-26 23:31:55');
INSERT INTO `transactions` VALUES (11, '37MM1GW6', 8, '2019-12-28 03:23:13', '2019-12-28 03:23:13');
INSERT INTO `transactions` VALUES (12, 'ZDMPX8AJ', 8, '2019-12-28 03:50:15', '2019-12-28 03:50:15');
INSERT INTO `transactions` VALUES (13, 'JZYZF6PE', 8, '2019-12-28 03:51:44', '2019-12-28 03:51:44');

-- ----------------------------
-- Table structure for transactions_detail
-- ----------------------------
DROP TABLE IF EXISTS `transactions_detail`;
CREATE TABLE `transactions_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactions_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `subtotal` double NOT NULL,
  `create` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of transactions_detail
-- ----------------------------
INSERT INTO `transactions_detail` VALUES (1, 1, 28, 2, 20000, '2019-12-17 15:57:49');
INSERT INTO `transactions_detail` VALUES (2, 1, 27, 2, 180000, '2019-12-17 15:57:49');
INSERT INTO `transactions_detail` VALUES (3, 2, 28, 2, 20000, '2019-10-19 11:44:29');
INSERT INTO `transactions_detail` VALUES (4, 3, 27, 1, 90000, '2019-11-23 11:55:19');
INSERT INTO `transactions_detail` VALUES (5, 4, 27, 1, 90000, '2019-12-22 23:26:50');
INSERT INTO `transactions_detail` VALUES (6, 5, 28, 2, 20000, '2019-12-23 11:57:08');
INSERT INTO `transactions_detail` VALUES (7, 6, 27, 1, 90000, '2019-12-23 13:58:38');
INSERT INTO `transactions_detail` VALUES (8, 7, 29, 2, 26000, '2019-12-23 15:04:39');
INSERT INTO `transactions_detail` VALUES (9, 8, 29, 3, 39000, '2019-12-23 15:26:46');
INSERT INTO `transactions_detail` VALUES (11, 10, 27, 1, 90000, '2019-12-26 23:31:55');
INSERT INTO `transactions_detail` VALUES (12, 11, 29, 4, 52000, '2019-12-28 03:23:14');
INSERT INTO `transactions_detail` VALUES (13, 12, 28, 2, 20000, '2019-12-28 03:50:15');
INSERT INTO `transactions_detail` VALUES (14, 13, 28, 1, 10000, '2019-12-28 03:51:44');

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES (1, 'pcs', '2019-10-04 22:37:09');
INSERT INTO `unit` VALUES (2, 'pack', '2019-10-12 21:49:00');
INSERT INTO `unit` VALUES (3, 'sachet', '2019-11-19 10:52:00');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `phone` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `privilege` int(5) NOT NULL COMMENT '0 = admin, 1 = kasir, 2 = petugas',
  `status` int(5) NOT NULL COMMENT '0 = nonaktif, 1 = aktif',
  `create` datetime(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (5, 'Chitanda Eru', '081905241265', 'chitanda@admin', '123456', 0, 1, '2019-10-28 00:22:20');
INSERT INTO `users` VALUES (6, 'Kamado Tanjiro', '0888764536', 'tanjiro@cashier', '123456', 1, 1, '2019-10-28 00:23:55');
INSERT INTO `users` VALUES (8, 'Ipuy', '0899976787', 'ipuy@cashier', 'ipuy@cashier', 1, 1, '2019-12-19 11:46:30');
INSERT INTO `users` VALUES (9, 'Rifqi Maulatur', '081905241265', 'rifqi@officer', 'nasikucing', 2, 1, '2019-12-23 14:53:50');

-- ----------------------------
-- View structure for average_transactions_permonth
-- ----------------------------
DROP VIEW IF EXISTS `average_transactions_permonth`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `average_transactions_permonth` AS SELECT YEAR(total.date) as year, MONTHNAME(total.date) as month, SUM(total.total) as totalprice, MONTH(total.date) as numberofmonth, COUNT(total.id) as totaltrans, (SUM(total.total)/COUNT(total.id)) as average
FROM total_transactions_per_reference AS total
GROUP BY YEAR(total.date), MONTHNAME(total.date)
ORDER BY YEAR(total.date) ASC, MONTH(total.date) ASC ;

-- ----------------------------
-- View structure for detail_transactions_per_reference
-- ----------------------------
DROP VIEW IF EXISTS `detail_transactions_per_reference`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `detail_transactions_per_reference` AS SELECT detail.transactions_id, trans.no_reference,   pro.product_name, pro.sellingprice, detail.quantity, detail.subtotal FROM transactions_detail as detail 
INNER JOIN products as pro ON pro.id = detail.products_id
INNER JOIN transactions as trans ON trans.id = detail.transactions_id 
ORDER BY trans.date DESC,trans.no_reference ASC ;

-- ----------------------------
-- View structure for income_perday
-- ----------------------------
DROP VIEW IF EXISTS `income_perday`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `income_perday` AS SELECT YEAR(tot.date) as year, MONTH(tot.date) as month, MONTHNAME(tot.date) as nameofmonth, DAY(tot.date) as day, DAYNAME(tot.date) as nameofday, tot.date as date, SUM(tot.total) as total
FROM total_transactions_per_reference AS tot
GROUP BY YEAR(tot.date), MONTH(tot.date), DAY(tot.date)
ORDER BY YEAR(tot.date) ASC, MONTH(tot.date) ASC ;

-- ----------------------------
-- View structure for income_permonth
-- ----------------------------
DROP VIEW IF EXISTS `income_permonth`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `income_permonth` AS SELECT YEAR(total.date) as year, MONTHNAME(total.date) as month, SUM(total.total) as total, MONTH(total.date) as numberofmonth
FROM total_transactions_per_reference AS total
GROUP BY YEAR(total.date), MONTHNAME(total.date)
ORDER BY YEAR(total.date) ASC , MONTH(total.date) ASC ;

-- ----------------------------
-- View structure for outcome_permonth
-- ----------------------------
DROP VIEW IF EXISTS `outcome_permonth`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `outcome_permonth` AS SELECT YEAR(adj.date) as year, MONTHNAME(adj.date) as month, SUM(adj.total_price) as total, MONTH(adj.date) as numberofmonth
FROM products_adjustment as adj

GROUP BY YEAR(adj.date), MONTHNAME(adj.date)
ORDER BY YEAR(adj.date) ASC , MONTH(adj.date) ASC ;

-- ----------------------------
-- View structure for sum_total_transactions_permonth
-- ----------------------------
DROP VIEW IF EXISTS `sum_total_transactions_permonth`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `sum_total_transactions_permonth` AS SELECT YEAR(trans.date) as year, MONTHNAME(trans.date) as month, COUNT(trans.id) as totaltrans, MONTH(trans.date) as numberofmonth
FROM transactions as trans


GROUP BY YEAR(trans.date), MONTHNAME(trans.date)
ORDER BY YEAR(trans.date) ASC, MONTH(trans.date) ASC ;

-- ----------------------------
-- View structure for topsales_permonth
-- ----------------------------
DROP VIEW IF EXISTS `topsales_permonth`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `topsales_permonth` AS SELECT YEAR(transdetail.create) year, MONTHNAME(transdetail.`create`) as month, pro.product_name, SUM(transdetail.quantity) as topsales, u.unit_name, MONTH(transdetail.`create`) as numberofmonth


FROM transactions_detail as transdetail
INNER JOIN products as pro ON pro.id = transdetail.products_id
INNER JOIN unit as u ON u.id = pro.unit_id
GROUP BY  YEAR(transdetail.create), MONTHNAME(transdetail.`create`), transdetail.products_id
ORDER BY topsales DESC, YEAR(transdetail.create) ASC, MONTHNAME(transdetail.`create`)  ASC ;

-- ----------------------------
-- View structure for total_transactions_per_reference
-- ----------------------------
DROP VIEW IF EXISTS `total_transactions_per_reference`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `total_transactions_per_reference` AS SELECT trans.id, trans.users_id, us.name, trans.no_reference, trans.date, SUM(detail.subtotal) as total FROM transactions_detail as detail
INNER JOIN transactions as trans ON trans.id = detail.transactions_id
INNER JOIN users as us ON us.id = trans.users_id
GROUP BY trans.id
ORDER BY date DESC ;

-- ----------------------------
-- Triggers structure for table products_adjustment
-- ----------------------------
DROP TRIGGER IF EXISTS `update_add_stock`;
delimiter ;;
CREATE TRIGGER `update_add_stock` AFTER INSERT ON `products_adjustment` FOR EACH ROW BEGIN

UPDATE products as pro SET pro.stock = (pro.stock+(NEW.stock_adjustment))

WHERE pro.id = NEW.products_id;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table products_adjustment
-- ----------------------------
DROP TRIGGER IF EXISTS `update_drop_stock`;
delimiter ;;
CREATE TRIGGER `update_drop_stock` AFTER DELETE ON `products_adjustment` FOR EACH ROW BEGIN

UPDATE products as pro SET pro.stock = (pro.stock-(OLD.stock_adjustment))

WHERE pro.id = OLD.products_id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table transactions_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `update_stock_after_transaction`;
delimiter ;;
CREATE TRIGGER `update_stock_after_transaction` AFTER INSERT ON `transactions_detail` FOR EACH ROW BEGIN

UPDATE products as pro SET pro.stock = (pro.stock-(NEW.quantity))

WHERE pro.id = NEW.products_id;

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
