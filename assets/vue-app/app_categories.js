// const config = require('../../config.js');

var v = new Vue({
    el:'#app_categories',
    data:{
        url : 'http://localhost:3030/',
        categories:[],
        id: '',
        category_name:'',
        create:'',
        dataTable : null
    },
   created(){
        this.getCategories();
    },
    methods:{
       async getCategories (){
            try{
                const res = await axios.get(this.url+'categories');
                this.categories = res.data.data;
                console.log(res.data.data);
                
            }catch(err){
                console.log(err);
            }
        }
    }
});