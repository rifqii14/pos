<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <!-- General CSS Files -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/bootstrap/dist/css/bootstrap.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/fontawesome-free/css/all.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/sweetalert2/dist/sweetalert2.min.css' ?>">

<!-- CSS Libraries -->
<!-- <link rel="stylesheet" href="../node_modules/bootstrap-social/bootstrap-social.css"> -->


<!-- Template CSS -->
<link rel="stylesheet" href="<?= base_url().'/assets/css/style.css' ?>" >
<link rel="stylesheet" href="<?= base_url().'assets/css/components.css' ?>" >
</head>

<body>
  <!-- General JS Scripts -->
  <script src="<?= base_url().'assets/plugins/jquery/dist/jquery.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/popper.js/dist/popper.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/bootstrap/dist/js/bootstrap.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/jquery.nicescroll/dist/jquery.nicescroll.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/moment/min/moment.min.js'?>"></script>


  <script src="<?= base_url().'assets/js/stisla.js' ?> "></script>

  <!-- Template JS File -->
  <script src="<?= base_url().'assets/js/scripts.js' ?>" ></script>
  <script src="<?= base_url().'assets/js/custom.js' ?>"></script>

  <script src="<?= base_url().'assets/plugins/sweetalert2/dist/sweetalert2.min.js'?>"></script>

  <script type="text/javascript">

  let timerInterval;
        Swal.fire({
            title: 'Login Berhasil! <br> Terdeteksi sebagai Super Admin',
            html: 'Sistem akan meredirect dalam <b></b> detik.',
            timer: 2000,
            timerProgressBar: true,
            allowOutsideClick:false,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                Swal.getContent().querySelector('b')
                    .textContent = Swal.getTimerLeft()
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            if ( result.dismiss === Swal.DismissReason.timer ) {
                window.location.replace('../admin/dashboard') // eslint-disable-line
            }
        })
  </script>

</body>

</html>
