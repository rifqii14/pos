<?php $this->load->view('Admin/template/head_start'); ?>
<title>Admin | Dashboard</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->


<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Admin/template/nav_side'); ?>

    <!-- Main Content -->
    <div class="main-content">
      <section class="section">
        <div class="section-header">
          <h1>Dashboard</h1>
        </div>
        <div class="row">
          <!-- <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-primary">
                <i class="far fa-user"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Total Employees</h4>
                </div>
                <div class="card-body">
                  10
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-danger">
                <i class="far fa-newspaper"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Total Products</h4>
                </div>
                <div class="card-body">
                  42
                </div>
              </div>
            </div>
          </div> -->
          <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-success">
                <i class="fas fa-arrow-alt-circle-up"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Gross Sales (<?= $income['month']?>)</h4>
                </div>
                <div class="card-body">
                  Rp. <?= number_format($income['total']) ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-danger">
                <i class="fas fa-arrow-alt-circle-down"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Outcome (<?= $outcome['month']?>)</h4>
                </div>
                <div class="card-body">
                Rp. <?= number_format($outcome['total']) ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-primary">
                <i class="fas fa-chart-line"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Average Sales Per Transaction (<?= $average['month']?>)</h4>
                </div>
                <div class="card-body">
                Rp. <?= number_format($average['average']) ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <i class="fas fa-shopping-basket"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Total Transactions (<?= $totalTransactions['month']?>)</h4>
                </div>
                <div class="card-body">
                  <?= $totalTransactions['totaltrans'] ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-6 col-sm-6">
            <div class="card">
              <div class="card-header">
                <h4>Monthly Sales Amount (<?= date('Y')?>) </h4>
              </div>
              <div class="card-body">
                <canvas id="myChart" height="182"></canvas>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-6 col-sm-6">
          <div class="card">
              <div class="card-header">
                <h4>Top 5 Sales This Month</h4>
              </div>
              <div class="card-body">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>Product Name</th>
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; foreach($topsales as $top){ ?>
                      <tr>
                        <td scope="row"><?= $no++ ?></td>
                        <td scope="row"><?= $top['product_name'] ?></td>
                        <td scope="row"><?= $top['topsales'].' '.$top['unit_name'] ?></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                  
              </div>
            </div>
          </div>
          </div>
        </div>
      </section>
    </div>
		<?php $this->load->view('Admin/template/footer_start'); ?>
  </div>
</div>

<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
  <script src="<?= base_url().'assets/plugins/chart.js/dist/Chart.min.js' ?>"></script>

  <script type="text/javascript">
    var statistics_chart = document.getElementById("myChart").getContext('2d');

    var myChart = new Chart(statistics_chart, {
      type: 'bar',
      data: {
        labels: <?= $month ?>,
        datasets: [{
          backgroundColor: [

              "#56AEE2",
              "#5668E2",
              "#8A56E2",
              "#CF56E2",

              "#E256AE",
              "#E25668",
              "#E28956",
              "#E2CF56",

              "#AEE256",
              "#68E256",
              "#56E289",
              "#56E2CF"
              
          ],
          hoverBackgroundColor: [
              "#56AEE2",
              "#5668E2",
              "#8A56E2",
              "#CF56E2",

              "#E256AE",
              "#E25668",
              "#E28956",
              "#E2CF56",

              "#AEE256",
              "#68E256",
              "#56E289",
              "#56E2CF"
          ],
          data: <?= $result ?>

        }]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
                stepSize: 100000,
                callback: function(value) {
                  var ranges = [
                      { divider: 1e6, suffix: 'M' },
                      { divider: 1e3, suffix: 'k' }
                  ];
                  function formatNumber(n) {
                      for (var i = 0; i < ranges.length; i++) {
                        if (n >= ranges[i].divider) {
                            return (n / ranges[i].divider).toString() + ranges[i].suffix;
                        }
                      }
                      return n;
                  }
                  return 'Rp. ' + formatNumber(value);
                }
            }
          }]
        },
      }
    });
  </script>
<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>

