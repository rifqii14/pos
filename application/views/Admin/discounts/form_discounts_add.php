<style>
  .errors {   width: 100%;
  margin-top: 0.25rem;
  font-size: 90%;
  color: #dc3545;}
</style>

<div class="modal fade" id="modalAdd"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add new discount</h5>
			</div>
			<form class="needs-validations" method="POST" action="discounts/action/save">
				<div class="modal-body">
						<div class="form-group">
							<label>Promo Name</label>
							<div class="input-group">
							<input type="text" class="form-control"  name="discount_name">
							</div>
						</div>
						<div class="form-group">
							<label>Discount Percent</label>
							<div class="input-group">
								<input type="text" class="form-control" name="discount">
								<div class="invalid-feedback error">
									Discount cannot be Empty!
								</div>
								<div class="input-group-append">
									<div class="input-group-text">
										<i class="fas fa-percent"></i>
									</div>
								</div>
                    		</div>
                    	</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary" id="saveBtn">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">
	$('#saveBtn').on('click', function(e){
		$('.needs-validations').validate({
			errorClass: 'errors',
			rules:{
				'discount_name' : {required: true},
				'discount' : {required:true, number: true}
			},
			messages:{
				'discount_name' : {required: 'Discount name cannot be empty'},
				'discount' : {required:'Discount cannot be empty', number: 'Please fill with number'}
			}
		})
	});
</script>

