<div class="modal fade" id="modalEdit"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit discount</h5>
			</div>
			<form class="needs-validation" novalidate="" method="POST" action="discounts/action/edit">
				<div class="modal-body">
						<input type="hidden" class="form-control" required="" name="id" id="id_discount">   

						<div class="form-group">
							<label>Discounts Name</label>
							<input type="text" class="form-control" required="" name="discount_name" id="discount_name">
							<div class="invalid-feedback">
							discounts Name cannot be Empty!
							</div>
						</div>
						<div class="form-group">
							<label>Discount Percent</label>
							<div class="input-group">
								<input type="text" class="form-control" required="" name="discount" id="discount">
								<div class="invalid-feedback">
									Discount cannot be Empty!
								</div>
								<div class="input-group-append">
								<div class="input-group-text">
									<i class="fas fa-percent"></i>
								</div>

							</div>
                      </div>
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
