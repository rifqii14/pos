<?php $this->load->view('Admin/template/head_start'); ?>
<title>Admin | Discounts</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>">
<script src="<?= base_url().'assets/plugins/jquery/dist/jquery.min.js'?>"></script>

<style>
.modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    
}
</style>

<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Admin/template/nav_side'); ?>

<!-- Main Content -->
<div id="app_discounts">
	<div class="main-content">

	<section class="section">
		<div class="section-header">
		<h1>Discounts</h1>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<button type="button" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#modalAdd">
								<i class="fas fa-plus"></i> Add
							</button>

						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped" id="table-1">
									<thead>
									<tr>
										<th>No.</th>
										<th>Name</th>
                                        <th>Discount</th>
										<th width="20%">Action</th>
									</tr>
									</thead>
									<tbody id="show_data">
										<?php $no = 1; foreach($data as $discounts) {  ?>
										<tr>
											<td> <?= $no++ ?></td>
                                            <td> <?= $discounts['discount_name']; ?> </td>
											<td> <?= (1-$discounts['discount'])*100 ?> % </td>                                            
											<td> 
												<a href="javascript:;" class="btn btn-icon icon-left btn-info item-edit" data="<?= $discounts['id'] ?>">
													<i class="far fa-edit"></i>Edit
												</a>
												
												<a href="<?= base_url('admin/discounts/action/remove/'.$discounts['id'])?>" class="btn btn-icon icon-left btn-danger item-delete">
													<i class="fas fa-trash"></i>Delete
												</a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
</div>



<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
<script src="<?= base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js'?>"></script>
<script src="<?= base_url().'assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'?>"></script>
<script src="<?= base_url().'assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js'?>"></script>
<script src="<?= base_url().'assets/plugins/sweetalert/dist/sweetalert.min.js'?>"></script>
<script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js')?>"></script>

<?php include 'form_discounts_add.php' ?>
<?php include 'form_discounts_edit.php' ?>

<script type="text/javascript">
	$("#table-1").dataTable({
	  "columnDefs": [
	    { "sortable": true }
	  ]
	});

	$('#show_data').on('click','.item-edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('AdminController/DiscountsController/getId')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $.each(data,function(id, discount_name, discount){
                        $('#modalEdit').modal('show');
                        $('#discount_name').val(data.discount_name);
                        $('#discount').val(Math.round((1-data.discount)*100).toFixed(0));
						console.log(data.discount);
                        $('#id_discount').val(data.id);

                    });
					console.log(data);

                },
				error: function(xhr, stat, err){
					console.log(xhr.responseText);
				}
            });
            return false;
        });


	
	$(".item-delete").click(function(e) {
			e.preventDefault();
			var url = $(this).attr('href');
			swal({
				title: 'Are you sure?',
				text: 'Once deleted, you will not be able to recover this data',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						swal('Your data has been deleted!', {
							icon: 'success'
						}).then(()=>{
							window.location.replace(url);
						});

					} else {
						swal('Nothing to delete!',{
							icon: 'error'
						});
					}
			});
		});
</script>

<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>