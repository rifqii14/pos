<!-- General CSS Files -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/bootstrap/dist/css/bootstrap.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/fontawesome-free/css/all.css' ?>">

<!-- CSS Libraries -->
<!-- <link rel="stylesheet" href="../node_modules/bootstrap-social/bootstrap-social.css"> -->


<!-- Template CSS -->
<link rel="stylesheet" href="<?= base_url().'/assets/css/style.css' ?>" >
<link rel="stylesheet" href="<?= base_url().'assets/css/components.css' ?>" >

<script src="<?= base_url().'assets/plugins/axios/axios.min.js'?>"></script>
<script src="<?= base_url().'assets/plugins/vue/vue.min.js'?>"></script>




