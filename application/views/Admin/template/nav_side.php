<div class="main-sidebar  sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">TiPOS</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">Ti</a>
    </div>
    <ul class="sidebar-menu">
        <li class="<?= (current_url()==base_url('admin/dashboard')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'admin/dashboard' ?>"><i class="fas fa-home"></i> <span>Dashboard</span></a></li> 
        <li class="nav-item dropdown 
          <?php 
          if(current_url() == base_url('admin/categories') || current_url() == base_url('admin/discounts') || current_url() == base_url('admin/unit')|| current_url() == base_url('admin/products') || current_url() == base_url('admin/adjustment')) {
            echo  'active' ;
          }else{
            echo '';
          }?>">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-box-open"></i> <span>Library</span></a>
          <ul class="dropdown-menu">
            <li class="<?= (current_url() == base_url('admin/products')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'admin/products' ?>">Products</a></li>


            <li class="<?= (current_url() == base_url('admin/adjustment')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'admin/adjustment' ?>">Product Adjustment</a></li>


            <li class="<?= (current_url() == base_url('admin/categories')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'admin/categories' ?>">Categories</a></li>

            <li class="<?= (current_url() == base_url('admin/discounts')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'admin/discounts' ?>">Discounts</a></li>

            <li class="<?= (current_url() == base_url('admin/unit')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'admin/unit' ?>">Unit</a></li>

          </ul>
        </li>

        <?php if($this->session->userdata('privilege') == '0') {?>
        <li class="<?= (current_url() == base_url('admin/employees')) ? 'active':'' ?>" ><a class="nav-link" href="<?= base_url().'admin/employees' ?>"><i class="fas fa-users"></i> <span>Employees</span></a></li>
        <li class="<?= (current_url() == base_url('admin/data-transactions')) ? 'active':'' ?>" ><a class="nav-link" href="<?= base_url().'admin/data-transactions' ?>"><i class="fas fa-cart-plus"></i> <span>Transactions</span></a></li>
        
        <li class="nav-item dropdown 
          <?php 
          if(current_url() == base_url('admin/report-sales-permonth')) {
            echo  'active' ;
          }else{
            echo '';
          }?>">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-scroll"></i> <span>Reports</span></a>
          <ul class="dropdown-menu">
            <li class="<?= (current_url() == base_url('admin/report-sales-permonth')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'admin/report-sales-permonth' ?>">Sales Per Month</a></li>
          </ul>
        </li>
        
        <?php } ?>


      </ul>
  </aside>
</div>

