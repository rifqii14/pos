  <!-- General JS Scripts -->
  <script src="<?= base_url().'assets/plugins/jquery/dist/jquery.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/popper.js/dist/popper.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/bootstrap/dist/js/bootstrap.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/jquery.nicescroll/dist/jquery.nicescroll.min.js'?>"></script>

  <script src="<?= base_url().'assets/plugins/moment/min/moment.min.js'?>"></script>


  <script src="<?= base_url().'assets/js/stisla.js' ?> "></script>


  <!-- Template JS File -->
  <script src="<?= base_url().'assets/js/scripts.js' ?>" ></script>
  <script src="<?= base_url().'assets/js/custom.js' ?>"></script>
