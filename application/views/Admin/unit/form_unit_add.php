<div class="modal fade" id="modalAdd"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add new unit</h5>
			</div>
			<form class="needs-validation" novalidate="" method="POST" action="unit/action/save">
				<div class="modal-body">
						<div class="form-group">
							<label>Unit Name</label>
							<input type="text" class="form-control" required="" name="unit_name">
							<div class="invalid-feedback">
							Category Name cannot be Empty!
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
