<div class="modal fade" id="modalEdit"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit category</h5>
			</div>
			<form class="needs-validation" novalidate="" method="POST" action="categories/action/edit">
				<div class="modal-body">
						<input type="hidden" class="form-control" required="" name="id" id="id_category">   

						<div class="form-group">
							<label>Category Name</label>
							<input type="text" class="form-control" required="" name="category_name" id="category_name">
							<div class="invalid-feedback">
							Category Name cannot be Empty!
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
