<style>
  .errors {   width: 100%;
  margin-top: 0.30rem;
  font-size: 90%;
  color: #dc3545;}
</style>

<div class="modal fade" id="modalAdd"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add new adjustment</h5>
			</div>
			<form class="needs-validations" novalidate="" method="POST" action="adjustment/action/save">
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Product Name</label>
								<div class="input-group">
									<select class="form-control select2" name="product" id="productselect">
                                        <option value="0">-- CHOOSE PRODUCT --</option>
									<?php foreach($dataProducts as $products){ ?>
										<option value="<?= $products['id'] ?>"><?= $products['product_name'] ?> 
										<small><i>(current stock : <?= $products['stock']?>)<i></small>
									</option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>
                        <input type="hidden" class="form-control" name="productprice" id="productprice">

						<div class="col-sm-12">
							<div class="form-group">
								<label>Stock adjustment</label>
								<div class="input-group">
									<input type="text" class="form-control" name="stock" id="stock">
								</div>
							</div>
                        </div>
                        
                        <div class="col-sm-12">
							<div class="form-group">
								<label>Total Cost</label>
								<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">Rp. </span>
								</div>
									<input type="text" class="form-control" name="totalprice" id="totalprice">
								</div>
							</div>
						</div>						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary" id="saveBtn">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#productselect').on('change',function(){
            var id=$(this).val();
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('AdminController/AdjustmentController/getProductPrice')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $.each(data,function(id, costprice){
                        $('#productprice').val(data.costprice);
                        $('#totalprice').val(Math.round($('#stock').val()*$('#productprice').val()))
                    });
                },
				error: function(xhr, stat, err){
					console.log(xhr.responseText);
				}
            });
            return false;
        });

    $('#stock').on('keyup', function(e){
        $('#totalprice').val(Math.round($('#stock').val()*$('#productprice').val()))
    })
        
	$('#saveBtn').on('click', function(e){
		$('.needs-validations').validate({
			errorClass: 'errors',
			rules:{
				'totalprice' : {required:true, number: true},
				'stock' : {required:true, number: true}
			},
			messages:{
				'totalprice' : {required:'Total price cannot be empty', number:'Please fill with number'},
				'stock' : {required:'Stock cannot be empty', number:'Please fill with number'}
			}
		})
	});


</script>
