<?php $this->load->view('Admin/template/head_start'); ?>
<title>Admin | Report Sales Permonth</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css' ?>">


<style>
.modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    
}
</style>

<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Admin/template/nav_side'); ?>


<!-- Main Content -->
<div id="app_datas">
	<div class="main-content">
	<section class="section">
		<div class="section-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
                            <form id="formsearch">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label> Select Year </label>
                                            <input class="form-control year" name="year" id="year" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <button class="form-control btn btn-primary" id="search">Search</button>
                                        </div>
                                    </div>
                                    <?php if(isset($_GET['year'])){
                                        if(!empty($sales)) { ?>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <button class="form-control btn btn-warning" id="print"  type="button">Print</button>
                                            </div>
                                        </div>
                                    <?php } } ?>
                                </div>
                            </form>

                            <?php if(isset($_GET['year'])){ ?>
                                <?php if(!empty($sales)) {?>
                                    <center> <label><h2>Report Sales Per Month (<?= $_GET['year'] ?>)</h2></label> </center>
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="table-1">
                                            <thead>
                                            <tr>
                                                <th>Month</th>
                                                <th>Total Sales</th>
                                            </tr>
                                            </thead>
                                            <tbody id="show_data">
                                                <?php $no = 1; foreach($sales as $datas) {  ?>
                                                <tr>
                                                    <td> <?= $datas['month']; ?> </td>
                                                    <td> <?= "Rp. ".number_format($datas['total']) ; ?> </td>
                                                </tr>
                                                <?php }  ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } else {?>
                                    <table>
                                        <tr><td> NOT FOUND </td> </tr>
                                    </table>
                                <?php } ?>
                            <?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
</div>


<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
 <script src="<?= base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/sweetalert/dist/sweetalert.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js'?>"></script>
 <script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js')?>"></script>


 <?php 
 if(isset($_GET['year'])){
     $getyear = $_GET['year'];
 }else{
     $getyear = '';
 }
 ?>


<script type="text/javascript">
    $('.year').datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years",
        autoclose: true
    });

    $('#formsearch').submit(function(e){
        window.location.href = '<?= base_url().'admin/report-sales-permonths/'?>'+$('#year').val();
    });

    $('#print').on('click',function(e){
        window.location.href = '<?= base_url().'admin/report-sales-permonth/print?year='.$getyear ?>';
    });
        
</script>


<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>
