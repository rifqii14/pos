

<?php $this->load->view('Admin/template/head_start'); ?>
<title>Admin | Transactions #<?= $dataTrans['no_reference']; ?> Detail</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>">
<style>
.modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    
}
</style>

<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Admin/template/nav_side'); ?>


      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Invoice</h1>
          </div>

          <div class="section-body">
            <div class="invoice">
              <div class="invoice-print">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="invoice-title">
                      <h2>Invoice</h2>
                      <div class="invoice-number">Order #<?= $dataTrans['no_reference']; ?></div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-md-6">
                        <address>
                          <strong>No. Reference : </strong>#<?= $dataTrans['no_reference']; ?><br>
                          <strong>Cashier : </strong><?= $dataTrans['name']; ?><br>
                          <strong>Transactions Date : </strong><?= $this->general->humanDate($dataTrans['date']); ?><br>

                        </address>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="row mt-4">
                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover table-md">
                        <tr>
                          <th data-width="40">#</th>
                          <th>Item</th>
                          <th class="text-center">Price</th>
                          <th class="text-center">Quantity</th>
                          <th class="text-right">Subtotals</th>
                        </tr>
                        <?php $no = 1; foreach($dataDetail as $datas) {?>
                        <tr>
                          <td><?= $no++; ?></td>
                          <td><?= $datas['product_name']; ?></td>
                          <td class="text-center">Rp. <?= number_format($datas['sellingprice']); ?></td>
                          <td class="text-center"><?= $datas['quantity']; ?></td>
                          <td class="text-right">Rp. <?= number_format($datas['subtotal']); ?></td>
                        </tr>
                        <?php } ?>
                      </table>
                    </div>
                    <div class="row mt-4">
                      <div class="col-lg-12 text-right">
                        <hr class="mt-2 mb-2">
                        <div class="invoice-detail-item">
                          <div class="invoice-detail-name">Total</div>
                          <div class="invoice-detail-value invoice-detail-value-lg">Rp. <?= number_format($dataTrans['total']); ?></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>`
            </div>
          </div>
        </section>
      </div>


<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
 <script src="<?= base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/sweetalert/dist/sweetalert.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/printarea/printArea.js'?>"></script>



<script type="text/javascript">
	$("#table-1").dataTable({
	  "columnDefs": [
	    { "sortable": true }
	  ]
	});	
</script>


<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>

