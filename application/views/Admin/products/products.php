<?php $this->load->view('Admin/template/head_start'); ?>
<title>Admin | Products</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/select2/dist/css/select2.min.css' ?>">

<link rel="stylesheet" href="<?= base_url().'assets/plugins/jquery-selectric/selectric.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/ionicons201/css/ionicons.css' ?>">




<style>
.modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    
}
</style>

<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Admin/template/nav_side'); ?>



<!-- Main Content -->
<div id="app_products">
	<div class="main-content">

	<section class="section">
		<div class="section-header">
		<h1>Products</h1>
		</div>
		<?php 
			foreach($cek as $q){   
				echo "<div style='padding:5px' class='alert alert-light'>  <span class='ion-alert' > </span> Product name <a style='color:red'>". $q['product_name']."</a> is less than 10 stock. Please add more!</div>"; 
			}
		?>
		<div class="section-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<button type="button" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#modalAdd">
								<i class="fas fa-plus"></i> Add
							</button>

						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped" id="table-1"  style="table-layout:fixed;">
									<thead>
									<tr>
										<th width="7%">No.</th>
                                        <!-- <th>P</th> -->
                                        <th>Product Name</th>
                                        <th>Category</th>
										<th>Cost Price</th>                                        
                                        <th>Selling Price</th>
                                        <th>Discount</th>                                         
                                        <th>Final Price</th>
										<th>Stock</th>
										
										<th width="15%">Action</th>
									</tr>
									</thead>
									<tbody id="show_data">
										<?php $no = 1; foreach($data as $products) {  ?>
										<tr>
											<td> <?= $no++ ?></td>
											<!-- <td> <img width="50" src=<?= base_url($products['picture'])?>></td> -->

                                            <td> <?= $products['product_name']; ?> </td>
											<td> <?= $products['category_name']; ?> </td>
											<td> Rp. <?= number_format($products['costprice']); ?> </td>
											<td> Rp. <?= number_format($products['sellingprice']); ?> </td>
											<td> <?= (1-$products['discount'])*100  ?>% </td>
											<td> Rp. <?= number_format($products['finalprice']); ?> </td>
											<td width="10%"> 
												<?php if($products['stock'] >= 10) { ?>
												  <span class="badge badge-success"> <?= $products['stock'].' '.$products['unit_name'];  ?> </span>
												<?php } else if($products['stock'] >= 5) { ?>
												  <span class="badge badge-warning"> <?= $products['stock'].' '.$products['unit_name'];  ?> </span>
												<?php } else if($products['stock'] <=4 ) { ?>
												  <span class="badge badge-danger"> <?= $products['stock'].' '.$products['unit_name'];  ?> </span>
												<?php } ?>
											</td>

											
											<td> 
												<a href="javascript:;" class="btn btn-icon icon-left btn-info item-edit" data="<?= $products['idproduct'] ?>">
													<i class="far fa-edit"></i>
												</a>
												<a href="<?= base_url('admin/products/action/remove/'.$products['idproduct'])?>" class="btn btn-icon icon-left btn-danger item-delete">
													<i class="fas fa-trash"></i>
												</a>
											</td>
										</tr>
										<?php }  ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
</div>


<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
 <script src="<?= base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/sweetalert/dist/sweetalert.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/select2/dist/js/select2.full.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/jquery-selectric/jquery.selectric.min.js'?>"></script>
 <script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js')?>"></script>

 
 <?php include 'form_products_add.php' ?>
 <?php include 'form_products_edit.php' ?>


<script type="text/javascript">
	$("#table-1").dataTable({
	  "columnDefs": [
	    { "sortable": true }
	  ]
    });
    
    $('#modalAdd select').css('width', '100%');
    $('#modalEdit select').css('width', '100%');

	
	$('#show_data').on('click','.item-edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('AdminController/productsController/getId')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $.each(data,function(id, product_name, categories_id, barcode, picture, costprice, sellingprice, discounts_id, finalprice, stock, unit_id){
                        $('#modalEdit').modal('show');
                        $('#product_name_edit').val(data.product_name);
                        $('#idproduct').val(data.id);
                        $('#barcode_edit').val(data.barcode);
                        $('#pictexist').val(data.picture);
                        $('#costprice_edit').val(data.costprice);
                        $('#sellingprice_edit').val(data.sellingprice);
                        $('#finalprice_edit').val(data.finalprice);
                        $('#category_edit').val(data.categories_id).prop("selected",true).trigger('change');
                        $('#unit_edit').val(data.unit_id).prop("selected",true).trigger('change');
                        $('#discount_edit').val(data.discounts_id).prop("selected",true).trigger('change');
						$("#preview_edit").attr('src', '<?= base_url()?>'+data.picture);
						$('#productimg_edit').val('');
                    });
                },
				error: function(xhr, stat, err){
					console.log(xhr.responseText);
				}
            });
            return false;
        });

		$(".item-delete").click(function(e) {
			e.preventDefault();
			var url = $(this).attr('href');
			swal({
				title: 'Are you sure?',
				text: 'Once deleted, you will not be able to recover this data',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						swal('Your data has been deleted!', {
							icon: 'success'
						}).then(()=>{
							window.location.replace(url);
						});

					} else {
						swal('Nothing to delete!',{
							icon: 'error'
						});
					}
			});
		});
</script>


<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>
