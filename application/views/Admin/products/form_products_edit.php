<style>
  .errors {   width: 100%;
  margin-top: 0.30rem;
  font-size: 90%;
  color: #dc3545;}
</style>

<div class="modal fade" id="modalEdit"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit products</h5>
			</div>
			<!-- <form class="needs-validation" novalidate="" method="POST" action="products/action/save"> -->
			<form class="needs-validations" id="editform" novalidate="" action="products/action/edit" method="POST"  enctype="multipart/form-data" >
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Product Name</label>
								<div class="input-group">
									<input type="hidden" class="form-control" name="idproduct" id="idproduct">

									<input type="text" class="form-control" name="product_name" id="product_name_edit">
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<label>Category </label>
								<div class="input-group">
									<select class="form-control select2" name="category" id="category_edit">
									<?php foreach($dataCat as $categories){ ?>
										<option value="<?= $categories['id'] ?>" ><?= $categories['category_name'] ?></option>
									<?php }  ?>
                                    </select>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Barcode / Product Code</label>
								<div class="input-group">
									<input type="text" class="form-control" name="barcode" id="barcode_edit">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
                                <label>Picture</label>
							    <input type="hidden" class="form-control" name="pictexist" id="pictexist">
                                
								<input type="file" class="form-control" name="productimg" id="productimg_edit">
								<br><center><img id="preview_edit" src="" width="100"></center>

							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Cost price</label>
								<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">Rp </span>
								</div>
									<input type="text" class="form-control" name="costprice" id="costprice_edit">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Selling price</label>
								<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">Rp </span>
								</div>
									<input type="text" class="form-control" name="sellingprice" id="sellingprice_edit">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Discount</label>
								<div class="input-group">
									<select class="form-control select2" name="discount" id="discount_edit">
									<?php foreach($dataDisc as $discount){ ?>
										<option value="<?= $discount['id'] ?>" data-val="<?= $discount['discount'] ?>"><?= $discount['discount_name'] ?></option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Final price</label>
								<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1">Rp </span>
								</div>
									<input type="text" class="form-control" name="finalprice" id="finalprice_edit">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Unit</label>
								<div class="input-group">
									<select class="form-control select2" name="unit" id="unit_edit">
									<?php foreach($dataUnit as $unit){ ?>
										<option value="<?= $unit['id'] ?>"><?= $unit['unit_name'] ?></option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary" id="editBtn">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#editBtn').on('click', function(e){
		$('#editform').validate({
			errorClass: 'errors',
			rules:{
				'product_name' : {required: true},
				'barcode' : {required: true},
				'costprice' : {required:true, number: true},
				'sellingprice' : {required:true, number: true},
				'finalprice' : {required:true, number: true}
			},
			messages:{
				'product_name' : {required: 'Product name cannot be empty'},
				'barcode' : {required: 'Product Code cannot be empty'},
				'costprice' : {required:'Cost price cannot be empty', number:'Please fill with number'},
				'sellingprice' : {required:'Selling price cannot be empty', number:'Please fill with number'},
				'finalprice' : {required:'Final price cannot be empty', number:'Please fill with number'}
			}
		})
	});

	$('#discount_edit').on('change',function(e){

		var discountval = $(this).find(':selected').attr('data-val');
		$('#finalprice_edit').val(Math.round($('#sellingprice_edit').val()*discountval).toFixed(0));


	});

	$('#sellingprice_edit').on('keyup',function(e){
		var discountval = $('#discount_edit').find(':selected').attr('data-val');
		$('#finalprice_edit').val(Math.round($('#sellingprice_edit').val()*discountval).toFixed(0));
	})

	$( "#productimg_edit" ).change(function(e) {
          var ext = this.value.match(/\.([^\.]+)$/)[1];
          switch (ext) {
            case 'jpg':
            case 'png':
            case 'jpeg':
            case 'JPG':
	            case 'PNG':
            case 'JPEG':
              break;
            default:
              alert('Ekstensi File Tidak Diterima');
              this.value = '';
          }
    });

	function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#preview_edit').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#productimg_edit").change(function() {
    readURL(this);
    $('#preview_edit').css('display','block');
  });

</script>
