<style>
  .errors {   width: 100%;
  margin-top: 0.30rem;
  font-size: 90%;
  color: #dc3545;}
</style>

<div class="modal fade" id="modalAdd"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add new product</h5>
			</div>
			<!-- <form class="needs-validation" novalidate="" method="POST" action="products/action/save"> -->
			<form class="needs-validations" novalidate="" method="POST" action="products/action/save" enctype="multipart/form-data" >
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Product Name</label>
								<div class="input-group">
									<input type="text" class="form-control" name="product_name">
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<label>Category</label>
								<div class="input-group">
									<select class="form-control select2" name="category">
									<?php foreach($dataCat as $categories){ ?>
										<option value="<?= $categories['id'] ?>"><?= $categories['category_name'] ?></option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Barcode / Product Code</label>
								<div class="input-group">
									<input type="text" class="form-control" name="barcode" id="barcode">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Picture</label>
								<input type="file" class="form-control" name="productimg" id="productimg">
								<br><center><img id="preview" src="#"  width="100" style="display: none"></center>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Cost price</label>
								<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">Rp. </span>
								</div>
									<input type="text" class="form-control" name="costprice">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Selling price</label>
								<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text">Rp. </span>
								</div>
									<input type="text" class="form-control" name="sellingprice" id="sellingprice">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Discount</label>
								<div class="input-group">
									<select class="form-control select2" name="discount" id="discount">
									<?php foreach($dataDisc as $discount){ ?>
										<option value="<?= $discount['id'] ?>" data-val="<?= $discount['discount'] ?>"><?= $discount['discount_name'] ?></option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<label>Final price</label>
								<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1">Rp. </span>
								</div>
									<input type="text" class="form-control" name="finalprice" id="finalprice">
								</div>
							</div>
						</div>

						<!-- <div class="col-sm-6">
							<div class="form-group">
								<label>Stock</label>
								<div class="input-group">
									<input type="text" class="form-control" name="stock">
								</div>
							</div>
						</div> -->

						<div class="col-sm-6">
							<div class="form-group">
								<label>Unit</label>
								<div class="input-group">
									<select class="form-control select2" name="unit">
									<?php foreach($dataUnit as $unit){ ?>
										<option value="<?= $unit['id'] ?>"><?= $unit['unit_name'] ?></option>
									<?php } ?>
									</select>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary" id="saveBtn">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#saveBtn').on('click', function(e){
		$('.needs-validations').validate({
			errorClass: 'errors',
			rules:{
				'product_name' : {required: true},
				'barcode' : {required: true},
				'costprice' : {required:true, number: true},
				'sellingprice' : {required:true, number: true},
				'finalprice' : {required:true, number: true}
			},
			messages:{
				'product_name' : {required: 'Product name cannot be empty'},
				'barcode' : {required: 'Product Code cannot be empty'},
				'costprice' : {required:'Cost price cannot be empty', number:'Please fill with number'},
				'sellingprice' : {required:'Selling price cannot be empty', number:'Please fill with number'},
				'finalprice' : {required:'Final price cannot be empty', number:'Please fill with number'}			
			}
		})
	});

	$('#discount').on('change',function(e){

		var discountval = $(this).find(':selected').attr('data-val');
		$('#finalprice').val(Math.round($('#sellingprice').val()*discountval).toFixed(0));


	});

	$('#sellingprice').on('keyup',function(e){
		var discountval = $('#discount').find(':selected').attr('data-val');
		$('#finalprice').val(Math.round($('#sellingprice').val()*discountval).toFixed(0));
		
	})

	$( "#productimg" ).change(function(e) {
          var ext = this.value.match(/\.([^\.]+)$/)[1];
          switch (ext) {
            case 'jpg':
            case 'png':
            case 'jpeg':
            case 'JPG':
	            case 'PNG':
            case 'JPEG':
              break;
            default:
              alert('Ekstensi File Tidak Diterima');
              this.value = '';
          }
    });

	function readURLs(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#preview').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#productimg").change(function() {
    readURLs(this);
    $('#preview').css('display','block');
  });
</script>
