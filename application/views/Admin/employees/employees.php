<?php $this->load->view('Admin/template/head_start'); ?>
<title>Admin | Employees</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/select2/dist/css/select2.min.css' ?>">

<link rel="stylesheet" href="<?= base_url().'assets/plugins/jquery-selectric/selectric.css' ?>">



<style>
.modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    
}
</style>

<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Admin/template/nav_side'); ?>


<!-- Main Content -->
<div id="app_employees">
	<div class="main-content">
		<?php include 'form_employees_add.php' ?>
		<?php include 'form_employees_edit.php' ?>
	<section class="section">
		<div class="section-header">
		<h1>Employees</h1>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<button type="button" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#modalAdd">
								<i class="fas fa-plus"></i> Add
							</button>

						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped" id="table-1">
									<thead>
									<tr>
										<th>No.</th>
                                        <th>Name</th>
                                        <th>Phone</th>
										<th>Username</th>                                        
                                        <th>Privilege</th>
                                        <th>Status</th>                                         
										<th>Action</th>
									</tr>
									</thead>
									<tbody id="show_data">
										<?php $no = 1; foreach($data as $employees) {  ?>
										<tr>
											<td> <?= $no++ ?></td>
                                            <td> <?= $employees['name']; ?> </td>
											<td> <?= $employees['phone']; ?> </td>
											<td> <?= $employees['username']; ?> </td>
                                            <td> <?php if($employees['privilege'] == 0){ echo 'Admin'; } else if ($employees['privilege'] == 1){ echo 'Kasir'; } else { echo 'Petugas';}  ?> </td>
											<td> <?= (($employees['status'] == 1 ) ? '<div class="badge badge-success">Active</div>': '<div class="badge badge-danger">Deactive</div>')?> </td>
											<td> 
												<a href="javascript:;" class="btn btn-icon icon-left btn-info item-edit" data="<?= $employees['id'] ?>">
													<i class="far fa-edit"></i>Edit
												</a>
												
												<a href="<?= base_url('admin/employees/action/remove/'.$employees['id'])?>" class="btn btn-icon icon-left btn-danger item-delete">
													<i class="fas fa-trash"></i>Delete
												</a>
											</td>
										</tr>
										<?php }  ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
</div>


<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
 <script src="<?= base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/sweetalert/dist/sweetalert.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/select2/dist/js/select2.full.min.js'?>"></script>

 <script src="<?= base_url().'assets/plugins/jquery-selectric/jquery.selectric.min.js'?>"></script>


<script type="text/javascript">
	$("#table-1").dataTable({
	  "columnDefs": [
	    { "sortable": true }
	  ]
    });
    
    $('#modalAdd select').css('width', '100%');
	
	$('#show_data').on('click','.item-edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('AdminController/EmployeesController/getId')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $.each(data,function(id, name, phone, username, privilege, status){
                        $('#modalEdit').modal('show');
                        $('#name').val(data.name);
                        $('#phone').val(data.phone);
                        $('#username').val(data.username);
                        $('#name').val(data.name);
                        $('#id_employee').val(data.id);
                        $('#privilege').val(data.privilege).prop("selected",true).trigger('change').selectric('refresh');
                        $('#status').val(data.status).prop("selected",true).trigger('change').selectric('refresh');
                        
                    });
                    // console.log(data);
                    

                },
				error: function(xhr, stat, err){
					console.log(xhr.responseText);
				}
            });
            return false;
        });

		$(".item-delete").click(function(e) {
			e.preventDefault();
			var url = $(this).attr('href');
			swal({
				title: 'Are you sure?',
				text: 'Once deleted, you will not be able to recover this data',
				icon: 'warning',
				buttons: true,
				dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						swal('Your data has been deleted!', {
							icon: 'success'
						}).then(()=>{
							window.location.replace(url);
						});

					} else {
						swal('Nothing to delete!',{
							icon: 'error'
						});
					}
			});
		});
</script>


<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>
