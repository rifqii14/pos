<div class="modal fade" id="modalAdd"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add new employee</h5>
			</div>
			<form class="needs-validation" novalidate="" method="POST" action="employees/action/save">
				<div class="modal-body">
						<div class="form-group">
							<label>Employee Name</label>
							<input type="text" class="form-control" required="" name="name">
							<div class="invalid-feedback">
							Name cannot be Empty!
							</div>
                        </div>
                        <div class="form-group">
							<label>Phone</label>
							<input type="text" class="form-control" required="" name="phone">
							<div class="invalid-feedback">
							Phone cannot be Empty!
							</div>
                        </div>
                        <div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" required="" name="username">
							<div class="invalid-feedback">
							Username cannot be Empty!
							</div>
                        </div>
                        <div class="form-group">
							<label>Password</label>
							<input type="password" class="form-control" required="" name="password">
							<div class="invalid-feedback">
							Password cannot be Empty!
							</div>
                        </div>
                        <div class="form-group">
                            <label>Privilege</label>
                            <select class="form-control selectric" name="privilege">
                                <option value="0" selected>Admin</option>
                                <option value="1">Kasir</option>
                                <option value="2">Petugas</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control selectric" name="status">
                                <option value="1" selected>Active</option>
                                <option value="2">Deactive</option>
                            </select>
                        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
