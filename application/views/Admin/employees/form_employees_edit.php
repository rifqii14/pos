<div class="modal fade" id="modalEdit"  role="dialog"  aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add new employee</h5>
			</div>
			<form class="needs-validation" novalidate="" method="POST" action="employees/action/edit">
				<div class="modal-body">
						<input type="hidden" class="form-control" required="" name="id" id="id_employee">   

						<div class="form-group">
							<label>Employee Name</label>
							<input type="text" class="form-control" required="" name="name" id="name">
							<div class="invalid-feedback">
							Name cannot be Empty!
							</div>
                        </div>
                        <div class="form-group">
							<label>Phone</label>
							<input type="text" class="form-control" required="" name="phone" id="phone">
							<div class="invalid-feedback">
							Phone cannot be Empty!
							</div>
                        </div>
                        <div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" required="" name="username" id="username">
							<div class="invalid-feedback">
							Username cannot be Empty!
							</div>
                        </div>
                        <div class="form-group">
                            <label>Privilege</label>
                            <select class="form-control selectric" name="privilege" id="privilege">
                                <option value="0">Admin</option>
                                <option value="1">Kasir</option>
                                <option value="2">Petugas</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control selectric" name="status" id="status">
                                <option value="1">Active</option>
                                <option value="2">Deactive</option>
                            </select>
                        </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
