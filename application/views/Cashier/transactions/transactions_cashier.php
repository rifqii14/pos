<?php $this->load->view('Admin/template/head_start'); ?>
<title>Cashier | Products</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/select2/dist/css/select2.min.css' ?>">

<link rel="stylesheet" href="<?= base_url().'assets/plugins/jquery-selectric/selectric.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/ionicons201/css/ionicons.css' ?>">


<style>
.modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    
}
</style>

<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Cashier/template/nav_side'); ?>



<!-- Main Content -->
<div id="app_products">
	<div class="main-content">
		<section class="section">
			<div class="section-body">
				<div class="row">
					<div class="col-lg-6">
						<div class="card">
							<div class="card-body">
								<form class="needs-validations" id="transactionForm" >
		                				<input type="hidden" class="reset" id="productId" name="productId">
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
												<label>Barcode / Product Code</label>
							                      <div class="input-group mb-3">
							                        <input type="text" class="form-control reset" name="barcode" id="barcode">
							                        <div class="input-group-append">
							                          <button class="btn btn-primary" type="button" id="search">Search</button>
							                        </div>
							                      </div>
							                    </div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
												<label>Product Name</label>
							                        <input type="text" class="form-control reset" name="productname" id="productname" readonly>
							                     </div>
							                 </div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
												<label>Price</label>

							                      <div class="input-group mb-2">
							                        <div class="input-group-prepend">
							                          <div class="input-group-text">Rp.</div>
							                        </div>
							                        <input type="text" class="form-control reset" id="price" name="price" readonly >
							                      </div>
							                    </div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
												<label>Quantity</label>
							                      <div class="input-group mb-2">
							                        <input type="number" class="form-control reset" id="qty" name="qty">
							                        <div class="input-group-append">
							                          <div class="input-group-text badge badge-danger"><span class="badge badge-danger">In Stock : <span class="reset" id="maxqty">  </span> </span></div>
							                        </div>
							                      </div>
							                    </div>
							                 </div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
												<label>Subtotal</label>
							                      <div class="input-group mb-2">
							                        <div class="input-group-prepend">
							                          <div class="input-group-text">Rp.</div>
							                        </div>
							                        <input type="text" class="form-control reset" id="subtotal" name="subtotal" readonly >
							                      </div>
							                    </div>
											</div>
										</div>
									<div class="modal-footer">
										<button class="btn btn-primary btn-block" id="btnCart" type="button" hidden>Add to Cart</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="card">
							<div class="card-body">
									<div class="col-sm-12">
										<div class="form-group">
										<label><h2>Total : Rp. <span id="total"></span></h2></label>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
										<label>Pay</label>

					                      <div class="input-group mb-2">
					                        <div class="input-group-prepend">
					                          <div class="input-group-text">Rp.</div>
					                        </div>
					                        <input type="number" class="form-control reset" id="pay" name="pay" >
					                      </div>
					                    </div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
										<label><h2>Change : Rp. <span id="change"></span></h2></label>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section">
			<div class="section-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-body">
							<table id="table_transaksi" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th class="text-center">PRODUCT NAME</th>
										<th class="text-center">PRICE</th>
										<th class="text-center">QUANTITY</th>
										<th class="text-center">SUBTOTAL</th>
										<th class="text-center">ACTION</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							<button class="btn btn-primary btn-block" id="save">FINISH</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>


<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
 <script src="<?= base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/sweetalert/dist/sweetalert.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/select2/dist/js/select2.full.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/jquery-selectric/jquery.selectric.min.js'?>"></script>
 <script src="<?= base_url('assets/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
 <script src="<?= base_url('assets/plugins/jscoba/dynamictable.js')?>"></script>





<script type="text/javascript">

	$(document).ready(function() {
		$('#barcode').focus();

	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	})

	$('#search').on('click', function(e){
		e.preventDefault();
		var datas = {
			barcode: $('#barcode').val()
		}

		$.post( "<?php echo base_url('CashierController/TransactionsController/getProductByCode')?>", datas, function( data ) {

			  var hasil = $.parseJSON(data);
			  if(hasil == null){
			  	console.log(hasil);
			  	$('.reset').val('');
			  	$('.reset').text('');
			  	$('#barcode').focus();
				$('#btnCart').attr({"hidden": true});

			  }else{
			  	console.log(hasil);

			  	$('#productname').val(hasil.product_name);
			  	$('#price').val(hasil.sellingprice);
			  	$('#maxqty').text(hasil.stock);
			  	$('#productId').val(hasil.id);

			  	if(hasil.stock == 0){
			  		$('#qty').val(0).attr( {"readonly":true, "min":1, "max": hasil.stock} );
			  		$('#subtotal').val(0).attr({"readonly": true});
			  		$('#btnCart').attr({"hidden": true});
			  	}
			  	else{

			  		$('#qty').val(1).attr( {"readonly":false, "min":1, "max": hasil.stock} ).on('click keyup change', function() {
			  				$("#subtotal").val($('#price').val() * $('#qty').val());
			  			});

			  		$("#subtotal").val($('#price').val() * $('#qty').val());
			  		$('#btnCart').attr({"hidden": false});
			  	}
			  }
		});

	});

	$('#barcode').on('keyup', function(event){
		event.preventDefault();
		var keycode = (event.keyCode ? event.keyCode : event.which);

		if(keycode == '13'){
			var datas = {
				barcode: $('#barcode').val()
			}

			$.post( "<?php echo base_url('CashierController/TransactionsController/getProductByCode')?>", datas, function( data ) {

				  var hasil = $.parseJSON(data);
				  if(hasil == null){
				  	console.log(hasil);
				  	$('.reset').val('');
				  	$('#barcode').focus();
					$('#btnCart').attr({"hidden": true});
					$('.reset').text('');

				  }else{
				  	console.log(hasil);

				  	$('#productname').val(hasil.product_name);
				  	$('#price').val(hasil.sellingprice);
				  	$('#maxqty').text(hasil.stock);
				  	$('#productId').val(hasil.id);

				  	if(hasil.stock == 0){
				  		$('#qty').val(0).attr( {"readonly":true, "min":1, "max": hasil.stock} );
				  		$('#subtotal').val(0).attr({"readonly": true});
				  		$('#btnCart').attr({"hidden": true});
				  	}
				  	else{
				  		$('#qty').val(1).attr( {"readonly":false, "min":1, "max": hasil.stock} ).on('click keyup change', function() {
				  				$("#subtotal").val($('#price').val() * $('#qty').val());
				  			});

				  		$("#subtotal").val($('#price').val() * $('#qty').val());
				  		$('#btnCart').attr({"hidden": false});
				  	}
				  }
			});
		}
	});

	$('#pay').on('click keyup paste', function() {
			$("#change").text($('#pay').val() - $('#total').text());
		});

	var total = 0;
	var dTable = $('table').DynamicTable({
		tabData: {products_id:"",product_name:"", price:"", quantity: "", subtotal: ""},
		delButtonClass: "btn btn-block btn-danger",
		delButtonText: "<i class='fa fa-times'></i>",
		dataIdName: "products_id",
		hasFirstRow: true
	});

	dTable.setDelFunc(function(ob) {
		total -= parseInt(ob.subtotal);
		$("#total").text(total);
	});

	$('#btnCart').on('click',function(e) {
		e.preventDefault();
		if ($('#transactionForm').valid()){
			dTable.add({
				"products_id": $('#productId').val(),
				"product_name" : $('#productname').val(),
				"price" : $('#price').val(),
				"quantity": $('#qty').val(),
				"subtotal": $('#subtotal').val()
			}, function(e) {
				$('.reset').val(''); 
				$('.reset').text('');  
				total += parseInt(e.subtotal);
				$("#total").text(total);
			});
			$('#barcode').focus();
		}
	});



	$('#save').on('click',function(e) {
		e.preventDefault();
		$.post("<?php echo base_url('CashierController/TransactionsController/saveTransactions/') ?>", {send: JSON.stringify(dTable.getData())}, function(res) {
			alert("Berhasil");
			window.location.href= '<?= base_url()."cashier/data-transactions" ?>'
			dTable.clear();

		});
	});



</script>



<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>
