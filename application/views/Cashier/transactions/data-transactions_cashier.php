<?php $this->load->view('Admin/template/head_start'); ?>
<title>Cashier | Data Transactions</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>">
<link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>">
<style>
.modal-backdrop {
    /* bug fix - no overlay */    
    display: none;    
}
</style>

<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>

<?php $this->load->view('Admin/template/nav_header'); ?>
<?php $this->load->view('Cashier/template/nav_side'); ?>


<!-- Main Content -->
<div id="app_datas">
	<div class="main-content">
	<section class="section">
		<div class="section-header">
		<h1>Data Transactions</h1>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<!-- <div class="card-header">
							<button type="button" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#modalAdd">
								<i class="fas fa-plus"></i> Add
							</button>

						</div> -->
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped" id="table-1">
									<thead>
									<tr>
										<th>No.</th>
										<th>No. Reference</th>
                                        <th>Date Transactions</th>
                                        <th>Total</th>
                                        <th>Action</th>
									</tr>
									</thead>
									<tbody id="show_data">
										<?php $no = 1; foreach($dataTransactions as $datas) {  ?>
										<tr>
											<td> <?= $no++ ?></td>
                                            <td> <?= $datas['no_reference']; ?> </td>
                                            <td> <?= $this->general->humanDate( $datas['date']); ?> </td>
											<td> <?= "Rp. ".number_format($datas['total']) ; ?> </td>
											<td> 
                                                <a class="btn btn-icon icon-left btn-info" href="<?= base_url().'cashier/transactions/detail/'.$datas['id'] ?>">
                                                    Detail
                                                </a>
											</td>
										</tr>
										<?php }  ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
</div>


<?php $this->load->view('Admin/template/footer'); ?>
<!-- CUSTOM JS -->
 <script src="<?= base_url().'assets/plugins/datatables/media/js/jquery.dataTables.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/datatables.net-select-bs4/js/select.bootstrap4.min.js'?>"></script>
 <script src="<?= base_url().'assets/plugins/sweetalert/dist/sweetalert.min.js'?>"></script>


<script type="text/javascript">
	$("#table-1").dataTable({
	  "columnDefs": [
	    { "sortable": true }
	  ]
	});
	
</script>


<!-- END CUSTOM JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>
