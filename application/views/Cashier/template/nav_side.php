<div class="main-sidebar  sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="index.html">TiPOS</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">Ti</a>
    </div>
    <ul class="sidebar-menu">
        <li class="<?= (current_url()==base_url('cashier/add-transactions')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'cashier/add-transactions' ?>"><i class="fas fa-cart-plus"></i><span>Add Transactions</span></a></li> 
        <li class="<?= (current_url()==base_url('cashier/data-transactions')) ? 'active':'' ?>"><a class="nav-link" href="<?= base_url().'cashier/data-transactions' ?>"><i class="fas fa-book"></i><span>Data Transactions</span></a></li> 



      </ul>
  </aside>
</div>