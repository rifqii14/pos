<?php $this->load->view('Admin/template/head_start'); ?>
<title>Login</title>
<?php $this->load->view('Admin/template/head'); ?>

<!-- CUSTOM CSS -->
<style>
  .errors {   width: 100%;
  margin-top: 0.30rem;
  font-size: 90%;
  color: #dc3545;}
</style>

<!-- END CUSTOM CSS -->
<?php $this->load->view('Admin/template/head_end'); ?>


<div id="app">
  <section class="section">
    <div class="container mt-5">
      <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
          <div class="login-brand">
            <img src="<?=base_url().'assets/img/tiPos.png'?>" alt="logo" width="200">
          </div>

          <div class="card card-primary">
            <div class="card-header"><h4>Login</h4></div>

            <div class="card-body">
              <form method="POST" action="<?= base_url().'login/check' ?>" class="needs-validations" novalidate="">
                <div class="form-group">
                  <label for="username">Username</label>
                  <input id="username" type="text" class="form-control" name="username" tabindex="1" >
                  <errors></errors>
                </div>

                <div class="form-group">
                  <div class="d-block">
                  	<label for="password" class="control-label">Password</label>
                  </div>
                  <input id="password" type="password" class="form-control" name="password" tabindex="2" >
                  <errors></errors>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4" id="login">
                    Login
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="simple-footer">
            Copyright &copy; Rifqi Maulatur <br> Template by Stisla
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<?php $this->load->view('Admin/template/footer'); ?>
 <script src="<?php echo base_url('assets/plugins/jquery-validation/jquery.validate.min.js')?>"></script>

<!-- CUSTOM JS -->
<script type="text/javascript">

  $('#login').on('click', function(e){
    $('.needs-validations').validate({
      errorClass: 'errors',
      rules:{
        'username' : {required: true},
        'password' : {required: true}
      },
      messages:{
        'username' : {required: 'Username cannot be empty'},
        'password' : {required: 'Password cannot be empty'}    
      },
           errorElement: "span",
            errorPlacement: function(error, element) {
            element.siblings("errors").append(error);
        },
         highlight: function(element) {
            $(element).siblings("errors").addClass("errors");
        },
        unhighlight: function(element) {
            $(element).siblings("errors").removeClass("errors");
        }
    })
  });


</script>
<!-- END CUSTOM S=JS -->
<?php $this->load->view('Admin/template/footer_end'); ?>








