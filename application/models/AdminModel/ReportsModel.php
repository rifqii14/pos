<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReportsModel extends CI_Model {

    public function salesPerMonth($year){
        $this->db->where('year', $year);
        $query = $this->db->get('income_permonth');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

}

/* End of file ReportsModel.php */
