<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends CI_Model {

    public function totalEmployees(){
        $this->db->where(array('privilege !='=>0,'status'=>1));
        return $this->db->get('users');
    }

    public function incomePerMonth()
	{	
		$this->db->where('numberofmonth=month(now())');
		$this->db->where('year=year(now())');

		$retr = array();
	   	$retr = $this->db->get('income_permonth')->row();

	   	if ($retr != null ) {
	   		$ret = array(
	   			'numberofmonth' => $retr->numberofmonth,
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => $retr->year,
	   			'total' => $retr->total
	   			);
	   	}
	   	else{$ret = array(
	   			'numberofmonth' => date('m'),
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => date('Y'),
	   			'total' => '0'
	   		);}

	   	return $ret;
    }
    
    public function outcomePerMonth()
	{	
		$this->db->where('numberofmonth=month(now())');
		$this->db->where('year=year(now())');

		$retr = array();
	   	$retr = $this->db->get('outcome_permonth')->row();

	   	if ($retr != null ) {
	   		$ret = array(
	   			'numberofmonth' => $retr->numberofmonth,
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => $retr->year,
	   			'total' => $retr->total
	   			);
	   	}
	   	else{$ret = array(
	   			'numberofmonth' => date('m'),
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => date('Y'),
	   			'total' => '0'
	   		);}

	   	return $ret;
    }
    
    public function transPerMonth()
	{	
		$this->db->where('numberofmonth=month(now())');
		$this->db->where('year=year(now())');

		$retr = array();
	   	$retr = $this->db->get('sum_total_transactions_permonth')->row();

	   	if ($retr != null ) {
	   		$ret = array(
	   			'numberofmonth' => $retr->numberofmonth,
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => $retr->year,
	   			'totaltrans' => $retr->totaltrans
	   			);
	   	}
	   	else{$ret = array(
	   			'numberofmonth' => date('m'),
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => date('Y'),
	   			'totaltrans' => '0'
	   		);}

	   	return $ret;
	}

	public function avgPerMonth()
	{	
		$this->db->where('numberofmonth=month(now())');
		$this->db->where('year=year(now())');

		$retr = array();
	   	$retr = $this->db->get('average_transactions_permonth')->row();

	   	if ($retr != null ) {
	   		$ret = array(
	   			'numberofmonth' => $retr->numberofmonth,
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => $retr->year,
	   			'average' => $retr->average
	   			);
	   	}
	   	else{$ret = array(
	   			'numberofmonth' => date('m'),
	   			'month' => $this->general->humanDate3(date('F, Y')),
	   			'year' => date('Y'),
	   			'average' => 0
	   		);}

	   	return $ret;
	}

	public function chartSalesPerMonth(){
		$this->db->where('year=year(now())');
		$query = $this->db->get('income_permonth');
		return $query->result();
	}

	public function getMonthSales(){
		$query = $this->db->get('income_permonth');
		return $query->result();
	}

	public function topSalesPerMonth(){
		$this->db->where('numberofmonth=month(now())');
		$this->db->where('year=year(now())');
		$query = $this->db->get('topsales_permonth',5);
		return $query->result_array();
	}

}

/* End of file DashboardModel.php */

?>