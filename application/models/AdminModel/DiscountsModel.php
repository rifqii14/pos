<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DiscountsModel extends CI_Model {

    public function getDiscounts(){
        $query = $this->db->get('discounts');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function getDiscountsbyId($id){
        $this->db->where('id',$id);
        $hsl = $this->db->get('discounts');
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil = array(
                    'id' => $data->id,
                    'discount_name' => $data->discount_name,
                    'discount' => $data->discount                    );
            }
        }
        return $hasil;
    }

    public function postDiscounts($data){
        $query = $this->db->insert('discounts',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function updateDiscounts($data,$id){
        $this->db->where('id',$id);
        $query = $this->db->update('discounts',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function deleteDiscounts($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('discounts');

        if($query){
            return true;
        }else{
            return false;
        }
    }
}

/* End of file DiscountsModel.php */
