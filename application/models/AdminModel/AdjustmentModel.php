<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class AdjustmentModel extends CI_Model {

    public function getAdjustment(){
        $this->db->select('proa.id as id_adjustment, proa.stock_adjustment, proa.date, proa.total_price, un.unit_name, pro.product_name');
        $this->db->join('products as pro','pro.id = proa.products_id','inner');
        $this->db->join('unit as un','un.id = pro.unit_id','inner');
        $this->db->order_by('proa.date','desc');
        $query = $this->db->get('products_adjustment as proa');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function getProducts(){
        $this->db->where('delete',NULL);
        
        $query = $this->db->get('products');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function getProductPrice($id){
        $this->db->where('id',$id);
        $this->db->where('delete',NULL);
        $hsl = $this->db->get('products');
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil = array(
                    'id' => $data->id,
                    'costprice' => $data->costprice);
            }
        }
        return $hasil;
    }

    public function getAdjustmentbyId($id){
        $this->db->where('id',$id);
        $hsl = $this->db->get('products_adjustment');
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil = array(
                    'id' => $data->id,
                    'discount_name' => $data->discount_name,
                    'discount' => $data->discount                    );
            }
        }
        return $hasil;
    }

    public function postAdjustment($data){
        $query = $this->db->insert('products_adjustment',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function deleteAdjustment($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('products_adjustment');

        if($query){
            return true;
        }else{
            return false;
        }
    }

}

/* End of file AdjustmentModel.php */
