<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionsModel extends CI_Model {

    public function getTransactionsAllUser(){
        $query = $this->db->get('total_transactions_per_reference');
        
        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function getTransactionsbyID($id){
        $this->db->where('id',$id);
        $query = $this->db->get('total_transactions_per_reference',1);
        if($query){
            return $query->row_array();
        }else{
            return false;
        }

    }

    public function getDetail($id){
        $this->db->where('transactions_id',$id);
        $query = $this->db->get('detail_transactions_per_reference');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }  

}

/* End of file TransactionsModel.php */
