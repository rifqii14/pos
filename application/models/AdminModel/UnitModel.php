<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class UnitModel extends CI_Model {

    public function getUnit(){
        $this->db->order_by('create','desc');	

        $query = $this->db->get('unit');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function getUnitbyId($id){
        $this->db->where('id',$id);
        $hsl = $this->db->get('unit');
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil = array(
                    'id' => $data->id,
                    'unit_name' => $data->unit_name
                );
            }
        }
        return $hasil;
    }

    public function postUnit($data){
        $query = $this->db->insert('unit',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function updateUnit($data,$id){
        $this->db->where('id',$id);
        $query = $this->db->update('unit',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function deleteUnit($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('unit');

        if($query){
            return true;
        }else{
            return false;
        }
    }

}

/* End of file UnitModel.php */
