<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CategoriesModel extends CI_Model {

    public function getCategories(){
        $this->db->order_by('create','desc');	
         $query = $this->db->get('categories');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function getCategoriesbyId($id){
        $this->db->where('id',$id);
        $hsl = $this->db->get('categories');
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil = array(
                    'id' => $data->id,
                    'category_name' => $data->category_name,
                    );
            }
        }
        return $hasil;
    }

    public function postCategories($data){
        $query = $this->db->insert('categories',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function updateCategories($data,$id){
        $this->db->where('id',$id);
        $query = $this->db->update('categories',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function deleteCategories($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('categories');

        if($query){
            return true;
        }else{
            return false;
        }
    }
}

/* End of file CategoriesModel.php */
