<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsModel extends CI_Model {
    public function getProducts(){
        $this->db->select('pro.id as idproduct, pro.product_name, cat.category_name, pro.barcode, pro.picture, pro.costprice, pro.sellingprice, dis.discount, pro.finalprice, pro.stock, u.unit_name, pro.create');
        $this->db->from('products pro');
        $this->db->join('categories cat', 'cat.id = pro.categories_id', 'inner');
        $this->db->join('discounts dis', 'dis.id = pro.discounts_id', 'inner');
        $this->db->join('unit u', 'u.id = pro.unit_id', 'inner');
        $this->db->where('delete',NULL);
        $this->db->order_by('pro.create','desc');	

        $query = $this->db->get();

       if($query){
           return $query->result_array();
       }else{
           return false;
       }
   }
   
   public function checkStock(){
       $query = $this->db->get_where('products', array('stock < ' => 10,'delete'=>NULL ));

       if($query){
           return $query->result_array();
       }else{
           return false;
       }
   }

   public function getProductsbyId($id){
       $this->db->where('id',$id);
       $hsl = $this->db->get('products');
       if($hsl->num_rows()>0){
           foreach ($hsl->result() as $data) {
               $hasil = array(
                   'id' => $data->id,
                   'product_name' => $data->product_name,
                   'categories_id' => $data->categories_id,
                   'barcode' => $data->barcode,
                   'picture' => $data->picture,
                   'costprice' => $data->costprice,
                   'sellingprice' => $data->sellingprice,
                   'discounts_id' => $data->discounts_id,
                   'finalprice' => $data->finalprice,
                   'stock' => $data->stock,
                   'unit_id' => $data->unit_id
                   );
           }
       }
       return $hasil;
   }

   public function postProducts($data){
       $query = $this->db->insert('products',$data);

       if($query){
           return true;
       }else{
           return false;
       }
   }

   public function updateProducts($data,$id){
       $this->db->where('id',$id);
       $query = $this->db->update('products',$data);

       if($query){
           return true;
       }else{
           return false;
       }
   }

   public function deleteProducts($id,$data){
       $this->db->where('id',$id);
       $query = $this->db->update('products',$data);

       if($query){
           return true;
       }else{
           return false;
       }
   }
}

/* End of file ProductsModel.php */
