<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeesModel extends CI_Model {

    public function getEmployees(){
        $query = $this->db->get('users');

        if($query){
            return $query->result_array();
        }else{
            return false;
        }
    }

    public function getEmployeesbyId($id){
        $this->db->where('id',$id);
        $hsl = $this->db->get('users');
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil = array(
                    'id' => $data->id,
                    'name' => $data->name,
                    'phone' => $data->phone,
                    'username' => $data->username,                  
                    'privilege' => $data->privilege,       
                    'status' => $data->status,                  
                );
            }
        }
        return $hasil;
    }

    public function postEmployees($data){
        $query = $this->db->insert('users',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function updateEmployees($data,$id){
        $this->db->where('id',$id);
        $query = $this->db->update('users',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function deleteEmployees($id){
        $this->db->where('id',$id);
        $query = $this->db->delete('users');

        if($query){
            return true;
        }else{
            return false;
        }
    }


}

/* End of file EmployeesModel.php */
