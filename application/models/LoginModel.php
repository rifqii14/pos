<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {

    var $table = 'users';

    function __construct() {
        parent::__construct();
    }

    function checkLogin($username, $password) {
        $this->db->select('*');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('status', '1');
        $query = $this->db->get($this->table, 1);

        if ($query->num_rows() == 1) {
            return $query->row_array();
        }
    }

}

/* End of file LoginModel.php */
