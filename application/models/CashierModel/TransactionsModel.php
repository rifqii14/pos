<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionsModel extends CI_Model {

	public function getProductByCode($code){
		$query = $this->db->get_where('products', array('barcode' => $code,'delete' => NULL));
		return $query->row();
	}

	public function insertTransactions($data){
		$this->db->insert('transactions', $data);
	}

	public function insertTransactionsDetail($data){
		$this->db->insert('transactions_detail', $data);
	}

}

/* End of file TransactionsModel.php */
/* Location: ./application/models/CashierModel/TransactionsModel.php */