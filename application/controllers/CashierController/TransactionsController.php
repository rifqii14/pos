<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionsController extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CashierModel/TransactionsModel','tm');
        $this->general->cekKasirLogin();
    }
    

    public function index()
    {
        $this->load->view('Cashier/transactions/transactions_cashier');
    }

    public function getProductByCode(){
    	$code = $this->input->post('barcode');
    	$datas = $this->tm->getProductByCode($code);

    	echo json_encode($datas);
    }

    public function saveTransactions(){
        $numberRef = $this->general->generateRandomCode(8);

		$this->db->trans_begin();

		$master['no_reference'] = $numberRef;
		$master['users_id'] = $this->session->userdata('id_user');
		$master['date'] = date('Y-m-d H:i:s');
		$master['create'] = date('Y-m-d H:i:s');
		$this->tm->insertTransactions($master);
		$table1_id=$this->db->insert_id();

		

		$data = (array)json_decode($this->input->post('send'));
		foreach($data as $row) {
		    $filter_data = array(
		    	"transactions_id" => $table1_id,
		        "products_id" => $row->products_id,
		        "quantity" => $row->quantity,
		        "subtotal" => $row->subtotal,
		        'create' => date('Y-m-d H:i:s')
		    );
           //Call the save method
           
           print_r($data);
		   $this->tm->insertTransactionsDetail($filter_data);
		}


		if ($this->db->trans_status() === FALSE) {
		    $this->db->trans_rollback();
		    echo json_encode("Failed to Save Data");
		    // echo json_encode("<script>alert('Gagal ');window.location.href='".base_url()."kasir/Penjualan'; </script>");   
		} else {
		    $this->db->trans_commit();
		    echo json_encode("Success!");
		    // echo "<script> alert('Berhasil');window.location.href='".base_url()."kasir/Order'; </script>";   
		}
		$this->db->trans_complete();
    }

}

/* End of file TransactionsController.php */
