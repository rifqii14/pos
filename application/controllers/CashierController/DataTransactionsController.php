<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class DataTransactionsController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('CashierModel/DataTransactionsModel','dm');
        
        $this->general->cekKasirLogin();
    }
    

    public function index()
    {
        $id = $this->session->userdata('id_user');
        $data['dataTransactions'] = $this->dm->getTransactionsbyUser($id);
        $this->load->view('Cashier/transactions/data-transactions_cashier',$data);
    }

    public function detail($id){
        $data['dataTrans'] = $this->dm->getTransactionsbyID($id);
        $data['idpen'] = $id;
        $data['dataDetail'] = $this->dm->getDetail($id);
        $this->load->view('Cashier/transactions/detail-transaction_cashier',$data);
    }

}

/* End of file DataTransactionsController.php */
