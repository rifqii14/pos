<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class PrintController extends CI_Controller {
    function __construct() {
        parent::__construct();

        include_once APPPATH.'../assets/plugins/fpdf182/fpdf.php';
        $this->load->model('CashierModel/DataTransactionsModel','dm');
    }

    public function cetak($id)
    {
        $transactions = $this->dm->getTransactionsbyID($id);
        $items = $this->dm->getDetail($id);
        $no =1;

        $pdf = new FPDF('l','mm','A5');
        $pdf->AddPage();

        //transactions
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(25,7,'No Reference : #'.$transactions['no_reference'],0,1);
        $pdf->Cell(25,7,'Cashier : '.$transactions['name'],0,1);
        $pdf->Cell(25,7,'Transactions Date : '.$this->general->humanDate($transactions['date']),0,1);
        $pdf->Cell(10,7,'',0,1);

        //Items
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(5,6,'#',1,0);
        $pdf->Cell(80,6,'Items',1,0);
        $pdf->Cell(50,6,'Price',1,0);
        $pdf->Cell(20,6,'Qty',1,0);
        $pdf->Cell(40,6,'Subtotals',1,1);
        $pdf->SetFont('Arial','',10);
        // $mahasiswa = $this->db->get('mahasiswa')->result();
        foreach ($items as $row){
            $pdf->Cell(5,6,$no++,1,0);
            $pdf->Cell(80,6,$row['product_name'],1,0);
            $pdf->Cell(50,6,'Rp '.number_format($row['sellingprice']),1,0);
            $pdf->Cell(20,6,$row['quantity'],1,0,'C');
            $pdf->Cell(40,6,'Rp '.number_format($row['subtotal']),1,1);
        }

        $pdf->Cell(155,7,'Total : ',1,0,'C');
        $pdf->Cell(40,7,'Rp '.number_format($transactions['total']),1,1);

        $pdf->Cell(190,30,'Terimakasih Telah Berbelanja',0,1,'C');
        
        $pdf->Output('I');

    }

}

/* End of file PrintController.php */
