<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('LoginModel','User');
	}
	
	function index(){
		$this->load->view('v_login');
		
	}

    function login() {
   
        $this->form_validation->set_rules('username', 'username', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_error_delimiters('', '<br/>');

        if ($this->form_validation->run() == TRUE) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user = $this->User->checkLogin($username, $password);

            if (!empty($user)) {
                $sessionData['id_user'] = $user['id'];
                $sessionData['username'] = $user['username'];
                $sessionData['name'] = $user['name'];
                $sessionData['privilege'] = $user['privilege'];
                $sessionData['is_login'] = TRUE;

                $this->session->set_userdata($sessionData);

                if ($this->session->userdata('privilege') == '0') {
                    // redirect('admin/dashboard');
                    $this->load->view('Alert/alert_admin');
                }
                else if ($this->session->userdata('privilege') == '1') {
                    // $this->session->sess_expiration = 14400;
                    // redirect('admin/dashboard');
                    $this->load->view('Alert/alert_kasir');
                }
                else if ($this->session->userdata('privilege') == '2') {
                    // $this->session->sess_expiration = 14400;
                    // redirect('admin/dashboard');
                    $this->load->view('Alert/alert_petugas');
                }
                

            }else{

            echo "<script>
                alert('Username/Password Salah atau Akun tidak aktif');
                window.location.href='".base_urL()."login';
                </script>";

            }
        }


        // $this->load->view('v_login');
    }

    function logout() {

        $this->session->sess_destroy();
        redirect('login');

    }

}

/* End of file LoginAdminController.php */
/* Location: ./application/controllers/LoginAdminController.php */