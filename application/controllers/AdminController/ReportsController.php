<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class ReportsController extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel/ReportsModel','rm');
        include_once APPPATH.'../assets/plugins/fpdf182/fpdf.php';

        
        //Do your magic here
    }
    

    public function salesPermonthIndex(){
        $year = $this->input->get('year');
        $data['sales'] = $this->rm->salesPerMonth($year);
        $this->load->view('Admin/reports/report_salespermonth',$data);
    }

    public function printSalesPermonth(){
        $year = $this->input->get('year');
        $sales = $this->rm->salesPerMonth($year);
        
        $pdf = new FPDF('l','mm','A5');
        $pdf->AddPage();

        $pdf->SetFont('Arial','B',25);
        $pdf->Cell(190,30,'REPORT SALES PERMONTH ('.$year.')',0,1,'C');
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(100,6,'Month',1,0,'C');
        $pdf->Cell(90,6,'Total Sales',1,1,'C');

        foreach($sales as $row){
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(100,6,$row['month'],1,0,'C');
            $pdf->Cell(90,6,'Rp '.number_format($row['total']),1,1,'C');
        }

        $pdf->Output('I','REPORT SALES PERMONTH('.$year.').pdf');

        
    }



}

/* End of file ReportsController.php */
