<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class AdjustmentController extends CI_Controller {

    public function __construct() {
      parent::__construct();
      $this->load->model('AdminModel/AdjustmentModel','am');
      if($this->session->userdata('privilege') == '0'){
        $this->general->cekAdminLogin();
      }else if($this->session->userdata('privilege') == '2'){
        $this->general->cekPetugasLogin();
      }
    }
    
    public function getProductPrice(){
		    $id = $this->input->get('id');
		    $data = $this->am->getProductPrice($id);
        echo json_encode($data);
    }

    public function index()
    {
        $data['data'] = $this->am->getAdjustment();
        $data['dataProducts'] = $this->am->getProducts();

        $this->load->view('Admin/product_adjustment/product_adjustment',$data);
    }

    public function save(){
      $data['products_id'] = $this->input->post('product');
      $data['stock_adjustment'] = $this->input->post('stock');
      $data['total_price'] = $this->input->post('totalprice');
      $data['date'] = date('Y-m-d H:i:s');
      $data['create'] = date('Y-m-d H:i:s');
  
      $this->db->trans_begin();
      $query = $this->am->postAdjustment($data);
      $this->db->trans_complete();
  
      if ($this->db->trans_status() === FALSE || !$query) {
        $this->db->trans_rollback();
        echo "<script>
            alert('Gagal disimpan karena kesalahan tertentu!');
            window.location.href='".base_url()."admin/adjustment';
            </script>";
      }
      else{
        $this->db->trans_commit();
        echo "<script>
            alert('Berhasil Disimpan!');
            window.location.href='".base_url()."admin/adjustment';
            </script>";
      }
    }

    public function remove($id){
      $query = $this->am->deleteAdjustment($id);

      if($query){
          redirect('admin/adjustment');
      }
  }


}

/* End of file AdjustmentController.php */
