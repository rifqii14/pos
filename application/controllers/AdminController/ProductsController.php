<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsController extends CI_Controller {

    public function __construct() {
		parent::__construct();
        $this->load->model('AdminModel/ProductsModel','pm');
        $this->load->model('AdminModel/CategoriesModel','cm');
        $this->load->model('AdminModel/DiscountsModel','dm');
		$this->load->model('AdminModel/UnitModel','um');
		if($this->session->userdata('privilege') == '0'){
			$this->general->cekAdminLogin();
		}else if($this->session->userdata('privilege') == '2'){
			$this->general->cekPetugasLogin();
		}
        
    }
    
    public function getId(){
		$id = $this->input->get('id');
		$data = $this->pm->getProductsbyId($id);
        echo json_encode($data);
        
	}

    public function index() {
        $data['data'] = $this->pm->getProducts();
        $data['dataCat'] = $this->cm->getCategories();
        $data['dataDisc'] = $this->dm->getDiscounts();
        $data['dataUnit'] = $this->um->getUnit();
        $data['cek'] = $this->pm->checkStock();

        $this->load->view('Admin/products/products',$data);
    }

    public function save(){
        $config['upload_path']   = './img/product/';
        $config['allowed_types'] = '*';
        $config['file_name'] = 'product_'.time();
        $this->load->library('upload',$config,'productupload');
        $this->productupload->initialize($config);
        $uploads = $this->productupload->do_upload('productimg');

        if($uploads || $uploads == NULL){
            $file = $this->productupload->data('file_name');
            
            if($uploads == NULL){
                $productimage 		= '/img/product/default.jpg'; 
            }else{
            $productimage 		= '/img/product/'.$file; 
            }

            $data['product_name'] = $this->input->post('product_name');
            $data['categories_id'] = $this->input->post('category');
            $data['barcode'] = $this->input->post('barcode');
            $data['picture'] = $productimage;
            $data['costprice'] = $this->input->post('costprice');
            $data['sellingprice'] = $this->input->post('sellingprice');
            $data['discounts_id'] = $this->input->post('discount');
            $data['finalprice'] = $this->input->post('finalprice');
            $data['stock'] = 0;
            $data['unit_id'] = $this->input->post('unit');
            $data['create'] = date('Y-m-d H:i:s');
            $data['delete'] = NULL;

    
            $this->db->trans_begin();
            $query = $this->pm->postProducts($data);
            $this->db->trans_complete();
    
            if ($this->db->trans_status() === FALSE || !$query) {
                $this->db->trans_rollback();
                echo "<script>
                        alert('Gagal disimpan karena kesalahan tertentu!');
                        window.location.href='".base_url()."admin/products';
                        </script>";
            }
            else{
                $this->db->trans_commit();
                echo "<script>
                        alert('Berhasil Disimpan!');
                        window.location.href='".base_url()."admin/products';
                        </script>";
            }  


        }else{
                print_r($this->productupload->display_errors());
        }
    }

    public function edit(){
        $config['upload_path']   = './img/product/';
        $config['allowed_types'] = '*';
        $config['file_name'] = 'product_'.time();
        $this->load->library('upload',$config,'productupload');
        $this->productupload->initialize($config);
        $uploads = $this->productupload->do_upload('productimg');

        if($uploads || $uploads == NULL){
            $file = $this->productupload->data('file_name');
            
            if($uploads == NULL){
                $productimage 		= $this->input->post('pictexist');
            }else{
                $productimage 		= '/img/product/'.$file; 
            }

            $id = $this->input->post('idproduct');
            $data['product_name'] = $this->input->post('product_name');
            $data['categories_id'] = $this->input->post('category');
            $data['barcode'] = $this->input->post('barcode');
            $data['picture'] = $productimage;
            $data['costprice'] = $this->input->post('costprice');
            $data['sellingprice'] = $this->input->post('sellingprice');
            $data['discounts_id'] = $this->input->post('discount');
            $data['finalprice'] = $this->input->post('finalprice');
            $data['unit_id'] = $this->input->post('unit');
    
            $this->db->trans_begin();
            $query = $this->pm->updateProducts($data,$id);
            $this->db->trans_complete();
    
            if ($this->db->trans_status() === FALSE || !$query) {
                $this->db->trans_rollback();
                echo "<script>
                        alert('Gagal disimpan karena kesalahan tertentu!');
                        window.location.href='".base_url()."admin/products';
                        </script>";
            }
            else{
                $this->db->trans_commit();
                echo "<script>
                        alert('Berhasil Disimpan!');
                        window.location.href='".base_url()."admin/products';
                        </script>";
            }  


        }else{
                print_r($this->productupload->display_errors());
        }
    }

    public function remove($id){
        $data['delete'] = date('Y-m-d H:i:s');
        $query = $this->pm->deleteProducts($id,$data);

        if($query){
            redirect('admin/products');
        }
    }
}

/* End of file ProductsController.php */
