<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeesController extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->model('AdminModel/EmployeesModel','em');
		$this->general->cekAdminLogin();
		
    }

    public function index() {
        $data['data'] = $this->em->getEmployees();
        $this->load->view('Admin/employees/employees',$data);
    }

    public function getId(){
		$id = $this->input->get('id');
		$data = $this->em->getEmployeesbyId($id);
        echo json_encode($data);
	}

    public function save(){
        $data['name'] = $this->input->post('name');
        $data['phone'] = $this->input->post('phone');
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['privilege'] = $this->input->post('privilege');
        $data['status'] = $this->input->post('status');
        $data['create'] = date('Y-m-d H:i:s');

		$this->db->trans_begin();
		$query = $this->em->postEmployees($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/employees';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Disimpan!');
					window.location.href='".base_url()."admin/employees';
					</script>";
        }
    }

    public function edit(){
		$id =  $this->input->post('id');
        $data['name'] = $this->input->post('name');
        $data['phone'] = $this->input->post('phone');
        $data['username'] = $this->input->post('username');
        $data['privilege'] = $this->input->post('privilege');
        $data['status'] = $this->input->post('status');

		$this->db->trans_begin();
		$query = $this->em->updateEmployees($data,$id);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/employees';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Diubah!');
					window.location.href='".base_url()."admin/employees';
					</script>";
		}
	}

	public function remove($id){
		$this->em->deleteEmployees($id);
		redirect('admin/employees');
	}

}

/* End of file EmployeesController.php */
