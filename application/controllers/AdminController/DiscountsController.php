<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DiscountsController extends CI_Controller {

            
    public function __construct() {
        parent::__construct();
		$this->load->model('AdminModel/DiscountsModel','dm');
		if($this->session->userdata('privilege') == '0'){
			$this->general->cekAdminLogin();
		}else if($this->session->userdata('privilege') == '2'){
			$this->general->cekPetugasLogin();
		}
		
    }

    public function index(){
        $data['data'] = $this->dm->getDiscounts();
        $this->load->view('Admin/discounts/discounts',$data);
	}
	
	public function getId(){
		$id = $this->input->get('id');
		$data = $this->dm->getDiscountsbyId($id);
        echo json_encode($data);
	}

    public function save(){
        $data['discount_name'] = $this->input->post('discount_name');
        $data['discount'] = 1-($this->input->post('discount')/100);

        $data['create'] = date('Y-m-d H:i:s');

		$this->db->trans_begin();
		$query = $this->dm->postDiscounts($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/discounts';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Disimpan!');
					window.location.href='".base_url()."admin/discounts';
					</script>";
		}

	}

	public function edit(){
		$id =  $this->input->post('id');
		$data['discount_name'] = $this->input->post('discount_name');
		$data['discount'] = 1-($this->input->post('discount')/100);


		$this->db->trans_begin();
		$query = $this->dm->updateDiscounts($data,$id);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/discounts';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Diubah!');
					window.location.href='".base_url()."admin/discounts';
					</script>";
		}
	}
	
	public function remove($id){
		$query = $this->dm->deleteDiscounts($id);

		if($query){
			redirect('admin/discounts');
		}
	}

}

/* End of file DiscountsController.php */
