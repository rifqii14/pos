<?php



defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionsController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel/TransactionsModel','dm');
        
        $this->general->cekAdminLogin();
    }
    

    public function index()
    {
        $data['dataTransactions'] = $this->dm->getTransactionsAllUser();
        $this->load->view('Admin/transactions/transactions_admin',$data);
    }

    public function detail($id){
        $data['dataTrans'] = $this->dm->getTransactionsbyID($id);
        $data['idpen'] = $id;
        $data['dataDetail'] = $this->dm->getDetail($id);
        $this->load->view('Admin/transactions/detail_transactions_admin',$data);
    }


}

/* End of file TransactionsController.php */
