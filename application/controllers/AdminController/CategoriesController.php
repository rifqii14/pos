<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CategoriesController extends CI_Controller {

	
	public function __construct() {
		parent::__construct();
		$this->load->model('AdminModel/CategoriesModel','cm');
		if($this->session->userdata('privilege') == '0'){
			$this->general->cekAdminLogin();
		}else if($this->session->userdata('privilege') == '2'){
			$this->general->cekPetugasLogin();
		}
		
	}
	
	public function index() {	
		$data['data'] = $this->cm->getCategories();
		$this->load->view('Admin/categories/categories',$data);
	}

	public function getId(){
		$id = $this->input->get('id');
		$data = $this->cm->getCategoriesbyId($id);
        echo json_encode($data);
	}

	public function save(){
		$data['category_name'] = $this->input->post('category_name');
		$data['create'] = date('Y-m-d H:i:s');

		$this->db->trans_begin();
		$query = $this->cm->postCategories($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/categories';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Disimpan!');
					window.location.href='".base_url()."admin/categories';
					</script>";
		}
	}

	public function edit(){
		$id =  $this->input->post('id');
		$data['category_name'] = $this->input->post('category_name');

		$this->db->trans_begin();
		$query = $this->cm->updateCategories($data,$id);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/categories';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Diubah!');
					window.location.href='".base_url()."admin/categories';
					</script>";
		}
	}

	public function remove($id){
		$query = $this->cm->deleteCategories($id);

		if($query){
			redirect('admin/categories');
		}
	}

}

/* End of file CategoriesController.php */
/* Location: ./application/controllers/AdminController/CategoriesController.php */