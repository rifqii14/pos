<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardAdminController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('AdminModel/DashboardModel','adm');
		if($this->session->userdata('privilege') == '0'){
			$this->general->cekAdminLogin();
		}else if($this->session->userdata('privilege') == '2'){
			$this->general->cekPetugasLogin();
		}
	}

	public function index()
	{
		// $data['totalEmployees'] = $this->adm->totalEmployees()->num_rows();
		$data['income'] = $this->adm->incomePerMonth();
		$data['outcome'] = $this->adm->outcomePerMonth();
		$data['totalTransactions'] = $this->adm->transPerMonth();
		$data['average'] = $this->adm->avgPerMonth();
		$data['topsales'] = $this->adm->topSalesPerMonth();

		$totalsales= $this->adm->chartSalesPerMonth();
		$datas = array();
		foreach ($totalsales as $tot) {
			$datas[] = $tot->total;
		}

		$month= $this->adm->getMonthSales();
		$months = array();
		foreach ($month as $tot) {
			$months[] = $tot->month;
		}
		
		$data['result'] = json_encode($datas);
		$data['month'] = json_encode($months);

		$this->load->view('Admin/v_dashboard',$data);
	}
	


}

/* End of file DashboardAdminController.php */
/* Location: ./application/controllers/AdminController/DashboardAdminController.php */