<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class UnitController extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('AdminModel/UnitModel','um');
		if($this->session->userdata('privilege') == '0'){
			$this->general->cekAdminLogin();
		}else if($this->session->userdata('privilege') == '2'){
			$this->general->cekPetugasLogin();
		}
		
	}
	
	public function index() {	
		$data['data'] = $this->um->getUnit();
		$this->load->view('Admin/unit/unit',$data);
	}

	public function getId(){
		$id = $this->input->get('id');
		$data = $this->um->getUnitbyId($id);
        echo json_encode($data);
	}

	public function save(){
		$data['unit_name'] = $this->input->post('unit_name');
		$data['create'] = date('Y-m-d H:i:s');

		$this->db->trans_begin();
		$query = $this->um->postUnit($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/unit';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Disimpan!');
					window.location.href='".base_url()."admin/unit';
					</script>";
		}
	}

	public function edit(){
		$id =  $this->input->post('id');
		$data['unit_name'] = $this->input->post('unit_name');

		$this->db->trans_begin();
		$query = $this->um->updateUnit($data,$id);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE || !$query) {
			$this->db->trans_rollback();
			echo "<script>
					alert('Gagal disimpan karena kesalahan tertentu!');
					window.location.href='".base_url()."admin/unit';
					</script>";
		}
		else{
			$this->db->trans_commit();
			echo "<script>
					alert('Berhasil Diubah!');
					window.location.href='".base_url()."admin/unit';
					</script>";
		}
	}

	public function remove($id){
		$query = $this->um->deleteUnit($id);

		if($query){
			redirect('admin/unit');
		}
	}

}

/* End of file UnitController.php */
