<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'LoginController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

///LOGIN FUNCTION
$route['login'] = 'LoginController';
$route['logout'] = 'LoginController/logout';
$route['login/check'] = 'LoginController/login';

//ADMIN
$route['admin/dashboard'] = 'AdminController/DashboardAdminController';

$route['admin/categories'] = 'AdminController/CategoriesController';
$route['admin/categories/action/save'] = 'AdminController/CategoriesController/save';
$route['admin/categories/action/edit'] = 'AdminController/CategoriesController/edit';
$route['admin/categories/action/remove/(:any)'] = 'AdminController/CategoriesController/remove/$1';

$route['admin/discounts'] = 'AdminController/DiscountsController';
$route['admin/discounts/action/save'] = 'AdminController/DiscountsController/save';
$route['admin/discounts/action/edit'] = 'AdminController/DiscountsController/edit';
$route['admin/discounts/action/remove/(:any)'] = 'AdminController/DiscountsController/remove/$1';

$route['admin/unit'] = 'AdminController/UnitController';
$route['admin/unit/action/save'] = 'AdminController/UnitController/save';
$route['admin/unit/action/edit'] = 'AdminController/UnitController/edit';
$route['admin/unit/action/remove/(:any)'] = 'AdminController/UnitController/remove/$1';

$route['admin/employees'] = 'AdminController/EmployeesController';
$route['admin/employees/action/save'] = 'AdminController/EmployeesController/save';
$route['admin/employees/action/edit'] = 'AdminController/EmployeesController/edit';
$route['admin/employees/action/remove/(:any)'] = 'AdminController/EmployeesController/remove/$1';

$route['admin/products'] = 'AdminController/ProductsController';
$route['admin/products/action/save'] = 'AdminController/ProductsController/save';
$route['admin/products/action/edit'] = 'AdminController/ProductsController/edit';
$route['admin/products/action/remove/(:any)'] = 'AdminController/ProductsController/remove/$1';

$route['admin/adjustment'] = 'AdminController/AdjustmentController';
$route['admin/adjustment/action/save'] = 'AdminController/AdjustmentController/save';
$route['admin/adjustment/action/edit'] = 'AdminController/AdjustmentController/edit';
$route['admin/adjustment/action/remove/(:any)'] = 'AdminController/AdjustmentController/remove/$1';

$route['admin/data-transactions'] = 'AdminController/TransactionsController';
$route['admin/transactions/detail/(:any)'] = 'AdminController/TransactionsController/detail/$1';

$route['admin/report-sales-permonth'] = 'AdminController/ReportsController/salesPermonthIndex';
$route['admin/report-sales-permonth/print'] = 'AdminController/ReportsController/printSalesPermonth';






//AJAX ROUTE
$route['admin/categories/get'] = 'AdminController/CategoriesController/get';


//CASHIER
$route['cashier/add-transactions'] = 'CashierController/TransactionsController';
$route['cashier/data-transactions'] = 'CashierController/DataTransactionsController';
$route['cashier/transactions/detail/(:any)'] = 'CashierController/DataTransactionsController/detail/$1';
$route['cashier/print/(:any)'] = 'CashierController/PrintController/cetak/$1';


//OFFICE
$route['officer/dashboard'] = 'AdminController/DashboardAdminController';



