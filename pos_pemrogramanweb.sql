-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2020 at 09:17 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_pemrogramanweb`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `average_transactions_permonth`
-- (See below for the actual view)
--
CREATE TABLE `average_transactions_permonth` (
`year` int(4)
,`month` varchar(9)
,`totalprice` double
,`numberofmonth` int(2)
,`totaltrans` bigint(21)
,`average` double
);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `create`) VALUES
(5, 'Makanan', '2019-09-07 13:34:50'),
(9, 'Cemilan', '2019-09-30 14:10:07'),
(11, 'Obat', '2019-10-02 20:11:07'),
(15, 'Minuman', '2019-10-02 21:52:14');

-- --------------------------------------------------------

--
-- Stand-in structure for view `detail_transactions_per_reference`
-- (See below for the actual view)
--
CREATE TABLE `detail_transactions_per_reference` (
`transactions_id` int(11)
,`no_reference` varchar(100)
,`product_name` varchar(100)
,`sellingprice` double
,`quantity` int(11)
,`subtotal` double
);

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` int(11) NOT NULL,
  `discount_name` varchar(100) NOT NULL,
  `discount` double NOT NULL,
  `create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `discount_name`, `discount`, `create`) VALUES
(1, 'No discount', 1, '2019-09-30 23:16:30'),
(2, 'Promo Weekend Discount 10%', 0.9, '2019-09-28 17:39:51'),
(3, 'Promo Rabu Ceria Discount 20%', 0.8, '2019-09-28 17:40:31'),
(5, 'Promo JuSa Discount 15%', 0.85, '2019-10-04 09:40:28'),
(17, 'Promo JuSa Discount 45%', 0.55, '2019-10-10 10:16:13');

-- --------------------------------------------------------

--
-- Stand-in structure for view `income_perday`
-- (See below for the actual view)
--
CREATE TABLE `income_perday` (
`year` int(4)
,`month` int(2)
,`nameofmonth` varchar(9)
,`day` int(2)
,`nameofday` varchar(9)
,`date` datetime
,`total` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `income_permonth`
-- (See below for the actual view)
--
CREATE TABLE `income_permonth` (
`year` int(4)
,`month` varchar(9)
,`total` double
,`numberofmonth` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `outcome_permonth`
-- (See below for the actual view)
--
CREATE TABLE `outcome_permonth` (
`year` int(4)
,`month` varchar(9)
,`total` double
,`numberofmonth` int(2)
);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `costprice` double NOT NULL,
  `sellingprice` double NOT NULL,
  `discounts_id` int(11) NOT NULL,
  `finalprice` double NOT NULL,
  `stock` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `create` datetime NOT NULL,
  `delete` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `categories_id`, `barcode`, `picture`, `costprice`, `sellingprice`, `discounts_id`, `finalprice`, `stock`, `unit_id`, `create`, `delete`) VALUES
(27, 'Indomie Kari Ayam', 5, '8976251402917', '/img/product/product_1576167806.jpg', 78000, 90000, 1, 90000, 139, 2, '2019-12-12 23:23:26', NULL),
(28, 'Sirup Marjan Pandan', 15, '98763518', '/img/product/product_1576168282.jpg', 8000, 10000, 1, 10000, 140, 1, '2019-12-12 23:31:22', NULL),
(29, 'Lays Rasa Rumput Laut', 9, '123456', '/img/product/product_1577088227.jpg', 8000, 13000, 1, 13000, 1, 1, '2019-12-23 15:03:47', NULL),
(30, 'Indomie Soto', 5, '089686010343', '/img/product/product_1577638658.png', 1500, 3000, 1, 3000, 9, 1, '2019-12-29 23:57:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_adjustment`
--

CREATE TABLE `products_adjustment` (
  `id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `stock_adjustment` int(11) NOT NULL,
  `total_price` double NOT NULL,
  `date` datetime NOT NULL,
  `create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `products_adjustment`
--

INSERT INTO `products_adjustment` (`id`, `products_id`, `stock_adjustment`, `total_price`, `date`, `create`) VALUES
(34, 28, 10, 80000, '2019-12-12 23:32:01', '2019-12-12 23:32:01'),
(35, 27, 2, 156000, '2019-12-22 23:23:25', '2019-12-22 23:23:25'),
(36, 27, 3, 234000, '2019-12-23 14:00:28', '2019-12-23 14:00:28'),
(37, 28, 10, 80000, '2019-12-23 14:01:06', '2019-12-23 14:01:06'),
(38, 29, 10, 80000, '2019-12-23 15:04:03', '2019-12-23 15:04:03');

--
-- Triggers `products_adjustment`
--
DELIMITER $$
CREATE TRIGGER `update_add_stock` AFTER INSERT ON `products_adjustment` FOR EACH ROW BEGIN

UPDATE products as pro SET pro.stock = (pro.stock+(NEW.stock_adjustment))

WHERE pro.id = NEW.products_id;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_drop_stock` AFTER DELETE ON `products_adjustment` FOR EACH ROW BEGIN

UPDATE products as pro SET pro.stock = (pro.stock-(OLD.stock_adjustment))

WHERE pro.id = OLD.products_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `sum_total_transactions_permonth`
-- (See below for the actual view)
--
CREATE TABLE `sum_total_transactions_permonth` (
`year` int(4)
,`month` varchar(9)
,`totaltrans` bigint(21)
,`numberofmonth` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `topsales_permonth`
-- (See below for the actual view)
--
CREATE TABLE `topsales_permonth` (
`year` int(4)
,`month` varchar(9)
,`product_name` varchar(100)
,`topsales` decimal(32,0)
,`unit_name` varchar(100)
,`numberofmonth` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `total_transactions_per_reference`
-- (See below for the actual view)
--
CREATE TABLE `total_transactions_per_reference` (
`id` int(11)
,`users_id` int(11)
,`name` varchar(100)
,`no_reference` varchar(100)
,`date` datetime
,`total` double
);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `no_reference` varchar(100) NOT NULL,
  `users_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `create` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `no_reference`, `users_id`, `date`, `create`) VALUES
(1, 'VMDODBO5', 6, '2019-12-17 15:57:49', '2019-12-17 15:57:49'),
(2, 'FON8285T', 6, '2019-10-19 11:44:29', '2019-10-19 11:44:29'),
(3, 'DJ6A228J', 6, '2019-11-23 11:55:19', '2019-11-23 11:55:19'),
(4, '8SF94SXW', 6, '2019-12-22 23:26:50', '2019-12-22 23:26:50'),
(5, 'F8G5NF3N', 6, '2019-12-23 11:57:08', '2019-12-23 11:57:08'),
(6, 'PGTE1EGG', 6, '2019-12-23 13:58:38', '2019-12-23 13:58:38'),
(7, 'Z799EXB8', 6, '2019-12-23 15:04:38', '2019-12-23 15:04:38'),
(8, 'RRNBS33O', 6, '2019-12-23 15:26:46', '2019-12-23 15:26:46'),
(10, 'YOJZCR03', 8, '2019-12-26 23:31:55', '2019-12-26 23:31:55'),
(11, '37MM1GW6', 8, '2019-12-28 03:23:13', '2019-12-28 03:23:13'),
(12, 'ZDMPX8AJ', 8, '2019-12-28 03:50:15', '2019-12-28 03:50:15'),
(13, 'JZYZF6PE', 8, '2019-12-28 03:51:44', '2019-12-28 03:51:44'),
(14, 'K9SARG88', 8, '2019-12-30 16:01:38', '2019-12-30 16:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `transactions_detail`
--

CREATE TABLE `transactions_detail` (
  `id` int(11) NOT NULL,
  `transactions_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `subtotal` double NOT NULL,
  `create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `transactions_detail`
--

INSERT INTO `transactions_detail` (`id`, `transactions_id`, `products_id`, `quantity`, `subtotal`, `create`) VALUES
(1, 1, 28, 2, 20000, '2019-12-17 15:57:49'),
(2, 1, 27, 2, 180000, '2019-12-17 15:57:49'),
(3, 2, 28, 2, 20000, '2019-10-19 11:44:29'),
(4, 3, 27, 1, 90000, '2019-11-23 11:55:19'),
(5, 4, 27, 1, 90000, '2019-12-22 23:26:50'),
(6, 5, 28, 2, 20000, '2019-12-23 11:57:08'),
(7, 6, 27, 1, 90000, '2019-12-23 13:58:38'),
(8, 7, 29, 2, 26000, '2019-12-23 15:04:39'),
(9, 8, 29, 3, 39000, '2019-12-23 15:26:46'),
(11, 10, 27, 1, 90000, '2019-12-26 23:31:55'),
(12, 11, 29, 4, 52000, '2019-12-28 03:23:14'),
(13, 12, 28, 2, 20000, '2019-12-28 03:50:15'),
(14, 13, 28, 1, 10000, '2019-12-28 03:51:44'),
(15, 14, 30, 1, 3000, '2019-12-30 16:01:38');

--
-- Triggers `transactions_detail`
--
DELIMITER $$
CREATE TRIGGER `update_stock_after_transaction` AFTER INSERT ON `transactions_detail` FOR EACH ROW BEGIN

UPDATE products as pro SET pro.stock = (pro.stock-(NEW.quantity))

WHERE pro.id = NEW.products_id;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `unit_name` varchar(100) NOT NULL,
  `create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `unit_name`, `create`) VALUES
(1, 'pcs', '2019-10-04 22:37:09'),
(2, 'pack', '2019-10-12 21:49:00'),
(3, 'sachet', '2019-11-19 10:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `privilege` int(5) NOT NULL COMMENT '0 = admin, 1 = kasir, 2 = petugas',
  `status` int(5) NOT NULL COMMENT '0 = nonaktif, 1 = aktif',
  `create` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `username`, `password`, `privilege`, `status`, `create`) VALUES
(5, 'Fajar', '081905241265', 'fajar@admin', '123456', 0, 1, '2019-10-28 00:22:20'),
(6, 'Kamado Tanjiro', '0888764536', 'tanjiro@cashier', '123456', 1, 1, '2019-10-28 00:23:55'),
(8, 'Ipuy', '0899976787', 'ipuy@cashier', '123456', 1, 1, '2019-12-19 11:46:30'),
(9, 'Rifqi Maulatur', '081905241265', 'rifqi@officer', 'nasikucing', 2, 1, '2019-12-23 14:53:50');

-- --------------------------------------------------------

--
-- Structure for view `average_transactions_permonth`
--
DROP TABLE IF EXISTS `average_transactions_permonth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `average_transactions_permonth`  AS  select year(`total`.`date`) AS `year`,monthname(`total`.`date`) AS `month`,sum(`total`.`total`) AS `totalprice`,month(`total`.`date`) AS `numberofmonth`,count(`total`.`id`) AS `totaltrans`,(sum(`total`.`total`) / count(`total`.`id`)) AS `average` from `total_transactions_per_reference` `total` group by year(`total`.`date`),monthname(`total`.`date`) order by year(`total`.`date`),month(`total`.`date`) ;

-- --------------------------------------------------------

--
-- Structure for view `detail_transactions_per_reference`
--
DROP TABLE IF EXISTS `detail_transactions_per_reference`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `detail_transactions_per_reference`  AS  select `detail`.`transactions_id` AS `transactions_id`,`trans`.`no_reference` AS `no_reference`,`pro`.`product_name` AS `product_name`,`pro`.`sellingprice` AS `sellingprice`,`detail`.`quantity` AS `quantity`,`detail`.`subtotal` AS `subtotal` from ((`transactions_detail` `detail` join `products` `pro` on((`pro`.`id` = `detail`.`products_id`))) join `transactions` `trans` on((`trans`.`id` = `detail`.`transactions_id`))) order by `trans`.`date` desc,`trans`.`no_reference` ;

-- --------------------------------------------------------

--
-- Structure for view `income_perday`
--
DROP TABLE IF EXISTS `income_perday`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `income_perday`  AS  select year(`tot`.`date`) AS `year`,month(`tot`.`date`) AS `month`,monthname(`tot`.`date`) AS `nameofmonth`,dayofmonth(`tot`.`date`) AS `day`,dayname(`tot`.`date`) AS `nameofday`,`tot`.`date` AS `date`,sum(`tot`.`total`) AS `total` from `total_transactions_per_reference` `tot` group by year(`tot`.`date`),month(`tot`.`date`),dayofmonth(`tot`.`date`) order by year(`tot`.`date`),month(`tot`.`date`) ;

-- --------------------------------------------------------

--
-- Structure for view `income_permonth`
--
DROP TABLE IF EXISTS `income_permonth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `income_permonth`  AS  select year(`total`.`date`) AS `year`,monthname(`total`.`date`) AS `month`,sum(`total`.`total`) AS `total`,month(`total`.`date`) AS `numberofmonth` from `total_transactions_per_reference` `total` group by year(`total`.`date`),monthname(`total`.`date`) order by year(`total`.`date`),month(`total`.`date`) ;

-- --------------------------------------------------------

--
-- Structure for view `outcome_permonth`
--
DROP TABLE IF EXISTS `outcome_permonth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `outcome_permonth`  AS  select year(`adj`.`date`) AS `year`,monthname(`adj`.`date`) AS `month`,sum(`adj`.`total_price`) AS `total`,month(`adj`.`date`) AS `numberofmonth` from `products_adjustment` `adj` group by year(`adj`.`date`),monthname(`adj`.`date`) order by year(`adj`.`date`),month(`adj`.`date`) ;

-- --------------------------------------------------------

--
-- Structure for view `sum_total_transactions_permonth`
--
DROP TABLE IF EXISTS `sum_total_transactions_permonth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sum_total_transactions_permonth`  AS  select year(`trans`.`date`) AS `year`,monthname(`trans`.`date`) AS `month`,count(`trans`.`id`) AS `totaltrans`,month(`trans`.`date`) AS `numberofmonth` from `transactions` `trans` group by year(`trans`.`date`),monthname(`trans`.`date`) order by year(`trans`.`date`),month(`trans`.`date`) ;

-- --------------------------------------------------------

--
-- Structure for view `topsales_permonth`
--
DROP TABLE IF EXISTS `topsales_permonth`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `topsales_permonth`  AS  select year(`transdetail`.`create`) AS `year`,monthname(`transdetail`.`create`) AS `month`,`pro`.`product_name` AS `product_name`,sum(`transdetail`.`quantity`) AS `topsales`,`u`.`unit_name` AS `unit_name`,month(`transdetail`.`create`) AS `numberofmonth` from ((`transactions_detail` `transdetail` join `products` `pro` on((`pro`.`id` = `transdetail`.`products_id`))) join `unit` `u` on((`u`.`id` = `pro`.`unit_id`))) group by year(`transdetail`.`create`),monthname(`transdetail`.`create`),`transdetail`.`products_id` order by sum(`transdetail`.`quantity`) desc,year(`transdetail`.`create`),monthname(`transdetail`.`create`) ;

-- --------------------------------------------------------

--
-- Structure for view `total_transactions_per_reference`
--
DROP TABLE IF EXISTS `total_transactions_per_reference`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `total_transactions_per_reference`  AS  select `trans`.`id` AS `id`,`trans`.`users_id` AS `users_id`,`us`.`name` AS `name`,`trans`.`no_reference` AS `no_reference`,`trans`.`date` AS `date`,sum(`detail`.`subtotal`) AS `total` from ((`transactions_detail` `detail` join `transactions` `trans` on((`trans`.`id` = `detail`.`transactions_id`))) join `users` `us` on((`us`.`id` = `trans`.`users_id`))) group by `trans`.`id` order by `trans`.`date` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `products_adjustment`
--
ALTER TABLE `products_adjustment`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `transactions_detail`
--
ALTER TABLE `transactions_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `products_adjustment`
--
ALTER TABLE `products_adjustment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `transactions_detail`
--
ALTER TABLE `transactions_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
